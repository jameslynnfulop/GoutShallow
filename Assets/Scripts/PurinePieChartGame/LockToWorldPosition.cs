﻿using UnityEngine;
using System.Collections;

public class LockToWorldPosition : MonoBehaviour 
{
	Vector3 startingPosition;

	//public Transform actualParent;
	// Use this for initialization
	void Awake () 
	{
		startingPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = startingPosition;
	}
}
