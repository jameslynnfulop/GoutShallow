﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScienceParticles : MonoBehaviour
{

	public PurinePieChartController controller;

	public float startScale;
	public float endScale;

	private Vector3 startingPosition;

	LockWorldScale lockWorldScale;


	void Start ()
	{
		startingPosition = transform.localPosition;
	}

	void OnEnable()
	{
		if (startingPosition != Vector3.zero)
			transform.localPosition = startingPosition;
		GetComponent<PerlinNoiseMovement>().samplingScale = 0.2f;

		//ockWorldScale = GetComponent<LockWorldScale>();
	}

	void Update ()
	{
		transform.localScale = Vector3.Lerp(new Vector3(startScale, startScale, startScale),
											new Vector3(endScale, endScale, endScale),
											controller.moleculeScale);

		/*if (controller.moleculeScale > 0.9f)
		{
			lockWorldScale.enabled = true;
		}
		else
		{
			lockWorldScale.enabled = false;
		}*/
	}

	public void FlyAway()
	{
		GetComponent<PerlinNoiseMovement>().samplingScale = 5;
	}
}
