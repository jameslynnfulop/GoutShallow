﻿using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

public class RadialDragger : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{

    public GameObject filler;
    public GameObject knob;
    private Button knobButton;

    public Text counter;

    private Vector3 startingPos;

    private Vector3 centerPos;

    private float distanceFromCenter;

    [Range(0,1)]
    public float fillAmount = 0;

    public PurineExogenousSource[] sources;

    public float maxDelta = 1;
    float currentAngle = 0;


    public Transform correctPosition;

    public PurinePieChartController purinePieChartController;
    private bool startGame = false;

    [Range(0,-360)]
    public float correctAngleInDraggerSpace = -216;

    private Image image;
    private Sprite isUp;
    public Sprite isDown;

    public Flipbook highlight;

    public BackButtonCallback backButtonCallback;

	void Awake()
    {
        startingPos = knob.transform.localPosition;
        centerPos = filler.transform.localPosition;
        latestDragPosition = startingPos;

        distanceFromCenter = Vector3.Distance(startingPos, centerPos);
        RadialDrag(new Vector2(startingPos.x, startingPos.y), 360, -5, true);

        knobButton = GetComponent<Button>();

        image = GetComponent<Image>();
        isUp = image.sprite;
	}

    Vector3 latestDragPosition;


	void Update ()
    {
        if (!knobButton.interactable)
        {
            RadialDrag(correctPosition.transform.localPosition, maxDelta, correctAngleInDraggerSpace);
            highlight.loop = false;
            return;
        }
        RadialDrag(latestDragPosition, maxDelta);
	}

    void OnDisable()
    {
        knob.transform.localPosition = startingPos;
        RadialDrag(new Vector2(startingPos.x, startingPos.y), 360, -5, true);
        latestDragPosition = startingPos;
        highlight.loop = true;
        startGame = false;
    }

    public void OnDrag(PointerEventData data)
    {
        Vector3 inputPos = Vector3.zero;
        inputPos.x = data.position.x;
        inputPos.y = data.position.y;
        inputPos = transform.parent.InverseTransformPoint(Camera.main.ScreenToWorldPoint(inputPos));
        inputPos.z = 0;
        latestDragPosition = inputPos;

        if (!startGame)
        {
            purinePieChartController.ToGame();
            startGame = true;
        }
    }

    public void OnPointerDown(PointerEventData data)
    {
        image.sprite = isDown;
        highlight.loop = false;
        backButtonCallback.ResetModuleOnBackButtonClick();
    }

    public void OnPointerUp(PointerEventData data)
    {
        image.sprite = isUp;
    }


    //Positions should be local to the knobs parent
    void RadialDrag(Vector3 position, float maxDelta, float inputAngle = 0, bool doFast=false)
    {

        float cartesianToPolarAngle = Mathf.Atan2(position.y, position.x) * Mathf.Rad2Deg;
        cartesianToPolarAngle = Mathf.MoveTowardsAngle(currentAngle, cartesianToPolarAngle, maxDelta);

        if (inputAngle != 0)
            cartesianToPolarAngle = Mathf.MoveTowards(currentAngle, inputAngle, maxDelta);

        if (inputAngle != 0 && doFast)
        {
            cartesianToPolarAngle = inputAngle;
        }

        currentAngle = Mathf.Clamp(cartesianToPolarAngle, -358, -2);

        //if (currentAngle < -2 && currentAngle > -358)
        {
            fillAmount = Mathf.Lerp(0, 1, Mathf.InverseLerp(0, -360, currentAngle));


            Vector3 newPos = Vector3.zero;
            newPos.x = distanceFromCenter * Mathf.Cos(Mathf.Deg2Rad * currentAngle);
            newPos.y = distanceFromCenter * Mathf.Sin(Mathf.Deg2Rad * currentAngle);


            knob.transform.localPosition = newPos;

            filler.GetComponent<Image>().fillAmount = fillAmount;

            int counterInt = Mathf.RoundToInt(Mathf.Lerp(1,0,fillAmount) * 100);
            counter.text = counterInt.ToString() + "%";

//            Debug.Log("fillamount: " + fillAmount);

        }

        for (int i = 0; i < sources.Length; i++)
        {
            sources[i].t = fillAmount;
        }
    }

    private void SetDraggedPosition(PointerEventData data)
    {

    }
}
