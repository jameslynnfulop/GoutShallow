﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class PurinePieChartController : MonoBehaviour
{
	private Animator animator;
	public Button dragger;

	public PurineGameController moleculeGame;

	public float moleculeScale = 0;
	public float scaleDuration = 5;

	public GameObject[] subpagesToTurnOffInTransition;

	public CanvasGroup myPurineBackground;
	//public CrossfadeCanvasGroup realPurineBackground;

//	public MotionBlur motionBlur;

	void Start ()
	{
		animator = GetComponent<Animator>();
	}

	void Update ()
	{

	}

	public void ToGame()
	{
		animator.SetBool("toGame", true);
	}

	public void ToResults()
	{
		animator.SetBool("isResults", true);
		dragger.interactable = false;
		StartCoroutine(ScaleScienceStuff());
	}

	void OnEnable()
	{
		myPurineBackground.alpha = 1;
	}

	void OnDisable()
	{
		animator.SetBool("isResults", false);
		dragger.interactable = true;
		moleculeScale = 0;
//		motionBlur.enabled = false;
	}

	public void SendEnableMessageToPurineMolecule()
	{
		moleculeGame.SendMessage("EnableBackground");
	}

	IEnumerator ScaleScienceStuff()
	{
		float startTime = Time.time;
		while (Time.time < startTime + scaleDuration)
		{
			moleculeScale = Mathf.Lerp(0, 1, (Time.time - startTime) / scaleDuration);
			yield return new WaitForEndOfFrame();
		}
	}

	public void RequestPurineMoleculeGameBackground()
	{
		moleculeGame.GetComponent<BackgroundRequester>().enabled = true;
		moleculeGame.GetComponent<BackgroundRequester>().RequestBackground();
//		StartCoroutine(CrossfadeOff(0.2f));
	}

	public void NiceToOff()
	{
//		motionBlur.enabled = true;
		foreach (GameObject go in subpagesToTurnOffInTransition)
		{
			GetComponent<PageAnimator>().AnimateOffSubpage(go.name);
		}
	}

	public void StartCrossfadeOff(float delay)
	{
		 StartCoroutine(CrossfadeOff(delay,0.85f));
	}

	public IEnumerator CrossfadeOff(float delay, float duration)
	{
		//CanvasGroup canvasgroup = GetComponent<CanvasGroup>();
		yield return new WaitForSeconds(delay);
        float startTime = Time.time;
        while (Time.time < startTime + duration + 0.05f)
        {
            float t = Mathf.InverseLerp(duration, 0, Time.time - startTime);
            myPurineBackground.alpha = t;
            yield return new WaitForEndOfFrame();
        }

	}
}
