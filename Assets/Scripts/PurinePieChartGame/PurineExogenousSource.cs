﻿using UnityEngine;
using System.Collections;

public class PurineExogenousSource : MonoBehaviour
{
	public SplineSection spline;
	public float startScale;
	public float endScale;
	[Range(0,1)]
	public float t;

	public float scaleToZeroAtT = 0.95f;

	public float mapMax = 1;
	// Use this for initialization
	void Start ()
	{

	}

	// Update is called once per frame
	void Update ()
	{
//        Debug.Log(t);
		float mappedT = Mathf.InverseLerp(0, mapMax, Mathf.Clamp01(t));
		transform.position = spline.GetPositionAt(Mathf.Lerp(0, 1, mappedT));
		transform.localScale = Vector3.Lerp(new Vector3(startScale,startScale,startScale),
											new Vector3(endScale,endScale,endScale),
											mappedT);

		if (t > mapMax)
		{
			transform.localScale = Vector3.Lerp(new Vector3(endScale,endScale,endScale),
												new Vector3(0,0,0),
												Mathf.Lerp(0, 1, Mathf.InverseLerp(mapMax, scaleToZeroAtT, t)));
		}
	}
}
