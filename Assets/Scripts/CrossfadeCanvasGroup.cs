﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CrossfadeCanvasGroup : MonoBehaviour 
{

    CanvasGroup canvasgroup;

    void Awake()
    {
        canvasgroup = GetComponent<CanvasGroup>();
    }

    public void CrossfadeOn(float duration)
    {
        canvasgroup = GetComponent<CanvasGroup>();
        if (canvasgroup.transform.parent != null)
            canvasgroup.transform.parent.gameObject.SetActive(true);//if they mask
        canvasgroup.gameObject.SetActive(true);
        canvasgroup.alpha = 0;
        StartCoroutine(FadeAlphaOn(canvasgroup, duration));
    }
    public void CrossfadeOff(float duration)
    {
        canvasgroup = GetComponent<CanvasGroup>();
        if (canvasgroup.transform.parent != null)
            canvasgroup.transform.parent.gameObject.SetActive(true);//if they mask
        canvasgroup.gameObject.SetActive(true);
        canvasgroup.alpha = 1;
        StartCoroutine(FadeAlphaOff(canvasgroup, duration));
    }

    public void SetFullAlpha()
    {
        if (canvasgroup != null)//null when we come here through main menu before normal path
            canvasgroup.alpha = 1;
    }

    public void SetNoAlpha()
    {
        if (canvasgroup != null)
            canvasgroup.alpha = 0;
    }

    IEnumerator FadeAlphaOn(CanvasGroup canvasgroup, float duration)
    {
        float startTime = Time.time;
        while (Time.time < startTime + duration + 0.05f)
        {
            float t = Mathf.InverseLerp(0, duration, Time.time - startTime);
            canvasgroup.alpha = t;
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator FadeAlphaOff(CanvasGroup canvasgroup, float duration)
    {
        float startTime = Time.time;
        while (Time.time < startTime + duration + 0.05f)
        {
            float t = Mathf.InverseLerp(duration, 0, Time.time - startTime);
            canvasgroup.alpha = t;
            yield return new WaitForEndOfFrame();
        }
    }

    public void OnDisable()
    {
        StopAllCoroutines();
    }
}

