﻿using UnityEngine;
using System.Collections;

public class EnableOnGameStart : MonoBehaviour {

    public GameObject[] objects;
	
	void Awake () 
    {
	    foreach(GameObject obj in objects)
        {
            obj.SetActive(true);
        }
	}
}
