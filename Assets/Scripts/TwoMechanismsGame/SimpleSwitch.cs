﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SimpleSwitch : MonoBehaviour 
{
    private Toggle toggle;
    public Image on;
    public Image off;

	void Start () 
    {
        toggle = GetComponent<Toggle>();
	}
	
	
	void Update () 
    {
	    if (toggle.isOn)
        {
            on.gameObject.SetActive(true);
            off.gameObject.SetActive(false);
        }
        else
        {
            on.gameObject.SetActive(false);
            off.gameObject.SetActive(true);
        }
	}
}
