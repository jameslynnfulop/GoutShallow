﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TwoMechanismsController : MonoBehaviour
{
	public Toggle inputValve;
    private bool inputValveStartingValue;

	public Toggle outputValve;
    private bool outputValveStartingValue;

	public float maxFillSpeed = 0.05f;
	public float maxEmptySpeed = -0.05f;

	private float fillAmount;

	public Transform backgroundMovie;
	public float minLocalPositionY;
	public float maxLocalPositionY;

	private DockBounceAccessor accessor;

	public ParticleSystem[] inputSystems;
	public ParticleSystem outputSystem;

	public ScrubbableFlipbook topCrystals;
	public ScrubbableFlipbook bottomCrystals;
	public GameObject waterLine;
	public float topCrystalsYOffsetFromWaterLine;
	public float topCrystalsMaxY;

    public float heavyEmissionRate = 3;
    public float heavyScale = 1;
    public float lightEmissionRate = 5;
    public float lightScale = 1;

    public ElementAnimator instructions;

    public AudioSource cracklingSound;

    public ElementAnimator submitButton;

	public GameObject[] highlights;

	public float crystalReductionPerSecond;

	public bool crackleStop;

    void Awake()
    {
        inputValveStartingValue = inputValve.isOn;
        outputValveStartingValue = outputValve.isOn;
        submitButton.Initialize();
    }

	void Start ()
	{
		accessor = GetComponent<DockBounceAccessor>();
        instructions.Initialize();
	}

	void OnEnable()
	{
		accessor = GetComponent<DockBounceAccessor>();
		accessor.TurnOnDockBounce();
		GetComponent<PageAnimator>().AllOn();
        inputValve.isOn = inputValveStartingValue;
        outputValve.isOn = outputValveStartingValue;
        fillAmount = 0;
        submitButton.Off();

		foreach (GameObject highlight in highlights)
		{
			highlight.SetActive(true);
			highlight.GetComponent<Flipbook>().imageIndex = 0;
			highlight.GetComponent<Image>().sprite = highlight.GetComponent<Flipbook>().sprites[0];
		}
		crackleStop = false;
		cracklingSound.volume = 0.7f;
	}

    void OnDisable()
    {
        instructions.On();
        cracklingSound.Stop();
    }

	private float increaseSpeed;
	private float decreaseSpeed;
	// Update is called once per frame
	void Update ()
	{
        float increaseSpeed = 0;
        float decreaseSpeed = 0;

		if (!inputValve.isOn && outputValve.isOn)//production/excretion
		{
			increaseSpeed = 0;
			decreaseSpeed = 0.5f;
		}
		else if (inputValve.isOn && outputValve.isOn)//over production/excertion
		{
			increaseSpeed = 0.5f;
			decreaseSpeed = 0;
		}
		else if (!inputValve.isOn && !outputValve.isOn)//production/inefficent exceretion
		{
			increaseSpeed = 0.5f;
			decreaseSpeed = 0;
		}
		else if (inputValve.isOn && !outputValve.isOn)//over production/inefficeint excertion
		{
			increaseSpeed = 1f;
			decreaseSpeed = 0;
		}

        if (inputValve.isOn)
        {
			foreach (ParticleSystem inputSystem in inputSystems)
			{
				inputSystem.emissionRate = heavyEmissionRate;
            	inputSystem.startSize = heavyScale;
			}
        }
        else
        {
			foreach (ParticleSystem inputSystem in inputSystems)
			{
				inputSystem.emissionRate = lightEmissionRate;
				inputSystem.startSize = lightScale;
			}
        }

        if (outputValve.isOn)
        {
            outputSystem.emissionRate = heavyEmissionRate;
            outputSystem.startSize = heavyScale;
        }
        else
        {
            outputSystem.emissionRate = lightEmissionRate;
            outputSystem.startSize = lightScale;
        }

        if (fillAmount > 0.3f && !submitButton.isOn)
        {
            StartCoroutine(submitButton.AnimateOn());
        }

		if (fillAmount > 0.3f && increaseSpeed > 0 && topCrystals.targetPercent < Mathf.InverseLerp(0.3f, 1, fillAmount))
		{
			topCrystals.targetPercent = Mathf.InverseLerp(0.3f, 1, fillAmount);
			bottomCrystals.targetPercent = Mathf.InverseLerp(0.3f, 1, fillAmount);
		}
        else if (fillAmount < 0.3f && decreaseSpeed > 0 && topCrystals.targetPercent > 0)//liquid going down and under half
		{
            topCrystals.targetPercent = Mathf.MoveTowards(topCrystals.targetPercent, 0, Time.deltaTime * crystalReductionPerSecond);
			bottomCrystals.targetPercent = Mathf.MoveTowards(bottomCrystals.targetPercent,0, Time.deltaTime * crystalReductionPerSecond);
		}

		waterLine.transform.localPosition = new Vector3(waterLine.transform.localPosition.x,
															backgroundMovie.transform.localPosition.y + topCrystalsYOffsetFromWaterLine,
															waterLine.transform.localPosition.z);
		//Debug.Log(waterLine.transform.localPosition.y + topCrystalsYOffsetFromWaterLine);
		if (waterLine.transform.localPosition.y > topCrystalsMaxY)
		{
			waterLine.transform.localPosition = new Vector3(waterLine.transform.localPosition.x,
																topCrystalsMaxY,
																waterLine.transform.localPosition.z);
		}

        decreaseSpeed *= maxEmptySpeed * Time.deltaTime;
		increaseSpeed *= maxFillSpeed * Time.deltaTime;
		fillAmount += increaseSpeed;
		fillAmount += decreaseSpeed;
		fillAmount = Mathf.Clamp01(fillAmount);


//        Debug.Log(fillAmount);
		backgroundMovie.localPosition = new Vector3(
			backgroundMovie.localPosition.x,
			Mathf.Lerp(minLocalPositionY, maxLocalPositionY, fillAmount),
			backgroundMovie.localPosition.z);

        if (increaseSpeed > 0 && !cracklingSound.isPlaying && (fillAmount > 0.3f && fillAmount < 1) && !crackleStop)
        {
			if (!fadingSound)
			{
					cracklingSound.Play();
			}


        }
        else if ((increaseSpeed == 0 || fillAmount == 0 || fillAmount == 1 || decreaseSpeed != 0) && cracklingSound.isPlaying || crackleStop)
        {
			if (!fadingSound)
			{
            	StartCoroutine(FadeSoundOut(cracklingSound, 0.2f));
			}
        }
	}

	private bool fadingSound = false;
	public IEnumerator FadeSoundOut(AudioSource sound, float duration)
	{
		float startTime = Time.time;
		float startVolume = sound.volume;
		fadingSound = true;
		while (Time.time < startTime + duration)
		{
			sound.volume = Mathf.Lerp(startVolume, 0, Mathf.InverseLerp(startTime, startTime + duration, Time.time));
			yield return new WaitForEndOfFrame();
		}
		sound.Pause();
		sound.volume = 0;
		fadingSound = false;
	}

	public void Stop()
	{
		fadingSound = true;
		StartCoroutine(FadeSoundOut(cracklingSound, 0.1f));
	}
}
