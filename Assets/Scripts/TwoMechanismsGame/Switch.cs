﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class Switch : MonoBehaviour, IDragHandler
{
	public GameObject handle;

	public float maxAngle = 30;

	[Range(0,1)]
	public float fillAmount = 0;

    private float startingAngle;
    
    public float maxDelta = 3;
    private float currentAngle = 0;

    public Transform particleCollider;
    public Transform onPosition;
    public Transform offPosition;
	void Start () 
	{
        startingAngle = handle.transform.localRotation.eulerAngles.y;
	}


    Vector3 latestDragPosition = new Vector3(-1,-1,-1);
	void Update () 
	{
        if (latestDragPosition == new Vector3(-1,-1,-1))
            return;

        PositionUpdate(latestDragPosition);
        particleCollider.transform.localPosition = Vector3.Lerp(offPosition.transform.localPosition, onPosition.transform.localPosition, fillAmount);
	}

    void Reset()
    {
        Vector3 eulerRotation = new Vector3(0, startingAngle, 0);
        handle.transform.localRotation = Quaternion.Euler(eulerRotation);
        latestDragPosition = new Vector3(-1,-1,-1);
    }
    
    void OnDisable()
    {
        Reset();
    }

	public void OnDrag(PointerEventData data)
    {
        Vector3 inputPos = transform.parent.InverseTransformPoint(Camera.main.ScreenToWorldPoint(new Vector3(data.position.x, data.position.y, 0)));
        inputPos.z = 0;
        latestDragPosition = inputPos;
    }


    void PositionUpdate(Vector3 position)
    {
        if (GetComponent<RectTransform>().rect.Contains(position))
        {

            float cartesianToPolarAngle = Mathf.Atan2(position.y, position.x) * Mathf.Rad2Deg;
            if (maxAngle < 0)
            {
                cartesianToPolarAngle = Mathf.Clamp(cartesianToPolarAngle, maxAngle, 0);    
            }
            else
            {
                cartesianToPolarAngle = Mathf.Clamp(cartesianToPolarAngle, 0, maxAngle);    
            }
            
            cartesianToPolarAngle = Mathf.MoveTowardsAngle(currentAngle, cartesianToPolarAngle, maxDelta);
            currentAngle = cartesianToPolarAngle;

            

            handle.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, currentAngle));

            fillAmount = Mathf.InverseLerp(0, maxAngle, currentAngle); 
        }
    }
}
