﻿using UnityEngine;
using System.Collections;

public class BackButton : MonoBehaviour {

    public GameObject mainMenu;
    public GameObject termsOfService;

    public ElementAnimator elementAnimator;

    void Awake()
    {
       // elementAnimator = GetComponent<ElementAnimator>();
        elementAnimator.Initialize();
        elementAnimator.Off();
        InvokeRepeating("Check", 0.5f, 0.25f);
    }


    bool offOnPurpose = false;
	void Check()
    {
        if ((mainMenu.activeSelf || termsOfService.activeSelf) && !elementAnimator.isAnimating && elementAnimator.gameObject.activeSelf)
        {
            //StopAllCoroutines();
            StartCoroutine(elementAnimator.AnimateOff());
        }
        else if (!elementAnimator.isOn && !elementAnimator.isAnimating && !elementAnimator.gameObject.activeSelf && (!mainMenu.activeSelf && !termsOfService.activeSelf) && !offOnPurpose)
        {
            //StopAllCoroutines();
            StartCoroutine(elementAnimator.AnimateOn());
        }
    }

    public void DisableForSecondsMethod(float seconds)
    {
        StartCoroutine(DisableForSeconds(seconds));
    }

    private IEnumerator DisableForSeconds(float seconds)
    {
        offOnPurpose = true;
        StartCoroutine(elementAnimator.AnimateOff());
        yield return new WaitForSeconds(seconds);
        StartCoroutine(elementAnimator.AnimateOn());
        offOnPurpose = false;
    }
}
