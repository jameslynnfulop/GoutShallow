using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class MultipaneSlideReferencesController : MonoBehaviour {

	private Button button;
    public GameObject[] subpanels;
    public Toggle[] panelIndicators;
    public GameObject nextButton;
    public GameObject previousButton;

    private int currentIndex;
    private int currentPanelCount;

	void Start ()
	{
        panelIndicators[0].gameObject.SetActive(true);
	}

    public void SetupReferencesForThisPage(int count)
    {
        currentPanelCount = count;
        for (int i = 0; i < subpanels.Length; i++)
        {
            if (i < count)
            {
                subpanels[i].SetActive(true);
                panelIndicators[i].gameObject.SetActive(true);
            }
            else
            {
                subpanels[i].SetActive(false);
                panelIndicators[i].gameObject.SetActive(false);
            }
        }
        currentIndex = 0;
        previousButton.SetActive(false);

        panelIndicators[0].isOn = true;
        subpanels[0].GetComponent<ElementAnimator>().On();
        for (int i = 1; i < panelIndicators.Length; i++ )
        {
            panelIndicators[i].isOn = false;
            subpanels[i].GetComponent<ElementAnimator>().Off();
        }

            if (count > 1)
            {
                nextButton.SetActive(true);
            }

    }

    public void Next()
    {
        previousButton.SetActive(true);

        panelIndicators[currentIndex].isOn = false;
        StartCoroutine(subpanels[currentIndex].GetComponent<ElementAnimator>().AnimateOffLeft());
        currentIndex++;
        panelIndicators[currentIndex].isOn = true;
        StartCoroutine(subpanels[currentIndex].GetComponent<ElementAnimator>().AnimateOnRight());

        if (currentIndex == currentPanelCount - 1)
            nextButton.SetActive(false);
    }

    public void Previous()
    {
        nextButton.SetActive(true);
        panelIndicators[currentIndex].isOn = false;
        StartCoroutine(subpanels[currentIndex].GetComponent<ElementAnimator>().AnimateOffRight());
        currentIndex--;
        panelIndicators[currentIndex].isOn = true;
        StartCoroutine(subpanels[currentIndex].GetComponent<ElementAnimator>().AnimateOnLeft());


        if (currentIndex == 0)
            previousButton.SetActive(false);
    }
}
