﻿using UnityEngine;
using System.Collections;

public class ResetAllMMTOnObject : MonoBehaviour 
{
    private MMTSaverResetter[] resetters;
    public GameObject objectToCollect;
	
	void Start () 
    {
        resetters = objectToCollect.GetComponentsInChildren<MMTSaverResetter>();
        foreach (MMTSaverResetter resetter in resetters)
            resetter.Init();
	}
	
	public void ResetAll()
    {
        resetters = objectToCollect.GetComponentsInChildren<MMTSaverResetter>();
        foreach (MMTSaverResetter resetter in resetters)
            resetter.Reset();
    }
}
