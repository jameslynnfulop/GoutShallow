﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MovieMatchCanvasGroupAlpha : MonoBehaviour
{

	public CanvasGroup canvasGroup;

	public RawImage rawImage;
	public Image image;
//	public MovileMovieTexture movie;

	public Sprite a;
	public Sprite b;


    public Mask mask;



	void Start ()
	{
		//image = GetComponent<RawImage>();


	}

	void Update ()
	{
        if (mask != null && image != null)
        {
            if (image.sprite == a)
                image.sprite = b;
            else
                image.sprite = a;
        }

		if (image != null)
			image.materialForRendering.SetFloat("_CanvasColor", canvasGroup.alpha);
		else if (rawImage != null)
			rawImage.materialForRendering.SetFloat("_CanvasColor", canvasGroup.alpha);
	}
}
