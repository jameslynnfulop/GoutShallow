﻿using UnityEngine;
using System.Collections;

public class BackgroundRequester : MonoBehaviour 
{
	public SharedBackgroundsManager backgroundsManager;

	public GameObject myBackground;

	public bool requestOnEnable = true;

    //public Transform transformToMatch;

    

	void OnEnable()
	{
		if (requestOnEnable)
			RequestBackground();
	}

	public void RequestBackground()
	{
		backgroundsManager.GetBackground(myBackground, transform);
	}

    public void Update()
    {
        myBackground.SetActive(true);
        myBackground.transform.localPosition = transform.localPosition;
    }

    public void ReleaseBackground()
    {

    }

    public void OnDisable()
    {
        myBackground.SetActive(false);
    }


}
