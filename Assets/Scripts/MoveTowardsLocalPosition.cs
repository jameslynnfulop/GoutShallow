﻿using UnityEngine;
using System.Collections;

public class MoveTowardsLocalPosition : MonoBehaviour
{
	public RectTransform topUICanvas;

	public float speed = 3;
	public Vector3 maxTargetPosition = Vector3.zero;
	public Vector3 minTargetPosition = Vector3.zero;

	public float maxScaleAtRatio = 1.33f;
	public float minScaleAtRatio = 1.77f;

	// Use this for initialization
	void Start ()
	{
		//targetPosition = transform.localPosition;
	}

	// Update is called once per frame
	void Update ()
	{
		float aspectRatio = topUICanvas.rect.width / topUICanvas.rect.height;
		float t = Mathf.InverseLerp(minScaleAtRatio, maxScaleAtRatio, aspectRatio);

		transform.localPosition = Vector3.MoveTowards(transform.localPosition, Vector3.Lerp(minTargetPosition, maxTargetPosition, t), Time.deltaTime * speed);
	}
}
