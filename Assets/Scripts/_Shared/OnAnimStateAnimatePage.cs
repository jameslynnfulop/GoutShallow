﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct AnimOptions
{
    public GameObject page;
    public bool instant;
    public bool on;
    public float delay;
}

[System.Serializable]
public struct AnimStateChange
{
    public string animationState;
    public AnimOptions[] pages;
    [HideInInspector]
    public bool hit;
}

public class OnAnimStateAnimatePage : MonoBehaviour 
{
    public AnimStateChange[] animStateChanges;

    private Animator animator;
    private PageAnimator pageAnimator;

    void OnEnable()
    {
        for (int i = 0; i < animStateChanges.Length; i++)
        {
            animStateChanges[i].hit = false;
        }
    }

	void Start () 
    {
        animator = GetComponent<Animator>();
        pageAnimator = GetComponent<PageAnimator>();
	}
	

	void Update () 
    {
        AnimatorStateInfo info = animator.GetCurrentAnimatorStateInfo(0);
        
        for (int i = 0; i < animStateChanges.Length; i++)
        {
            AnimStateChange currentSet = animStateChanges[i];
            if (info.IsName(currentSet.animationState) && !currentSet.hit)
            {
                for (int k = 0; k < currentSet.pages.Length; k++)
                {
                    AnimOptions pageSettings = currentSet.pages[k];
                    if (pageSettings.on)
                    {
                        if (pageSettings.instant)
                        {
                            pageAnimator.OnSubpage(pageSettings.page.name);
                        }
                        else
                        {
                            pageAnimator.AnimateOnSubpage(pageSettings.page.name, pageSettings.delay);
                        }
                    }
                    else
                    {
                        if (pageSettings.instant)
                        {
                            pageAnimator.OffSubpage(pageSettings.page.name);
                        }
                        else
                        {
                            pageAnimator.AnimateOffSubpage(pageSettings.page.name, pageSettings.delay);
                        }
                    }
                }
                animStateChanges[i].hit = true;
            }
        }
	    
	}
}
