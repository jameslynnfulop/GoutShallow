﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using TMPro;

[System.Serializable]
public struct TextContentPair
{
	public Text textArea;
	public TextMeshProUGUI textAreaPro;
	public TextAsset textAsset;
}

public class TextAreaTextAssetPairs : MonoBehaviour
{
	public TextContentPair[] pairs;


	// Use this for initialization
	void OnEnable ()
	{
		foreach (TextContentPair pair in pairs)
		{
			if (pair.textArea != null)
            	pair.textArea.text = pair.textAsset.text;

			if (pair.textAreaPro != null)
				pair.textAreaPro.text = pair.textAsset.text;
		}
	}

}
