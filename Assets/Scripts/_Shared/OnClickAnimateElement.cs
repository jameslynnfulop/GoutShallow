﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class OnClickAnimateElement : MonoBehaviour,
    IPointerDownHandler
{
    public enum AnimateOption
    {
        AnimateOn,
        AnimateOff
    }

    public AnimateOption animateOption;
    public ElementAnimator element;
    
    void Start () 
    {
	
	}
	
    public void OnPointerDown(PointerEventData data)
    {
        AnimateOther();
    }
	
    public void AnimateOther()
    {
        switch (animateOption)
        {
            case AnimateOption.AnimateOn:
                if (element.isOn == false)
                    StartCoroutine(element.AnimateOn());
                break;

            case AnimateOption.AnimateOff:
                StartCoroutine(element.AnimateOff());
                break;
        }
    }
}
