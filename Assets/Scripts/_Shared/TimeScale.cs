﻿using UnityEngine;
using System.Collections;

public class TimeScale : MonoBehaviour 
{
    //Component access to the timescale variable, 1 is default speed

    public float timeScale = 1;

	// Use this for initialization
	void Update ()
    {
#if !UNITY_EDITOR
        Time.timeScale = 1;
#else
        Time.timeScale = timeScale;
#endif
	}
	
}
