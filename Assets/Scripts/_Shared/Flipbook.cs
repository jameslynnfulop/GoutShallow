﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Image))]
public class Flipbook : MonoBehaviour 
{
	public Sprite[] sprites;
	public int framesToSkip = 0;
	public bool loop = false;
	public bool disableOnFinish = false;

	public bool playForward = true;
	Image image;

	[HideInInspector]
	public int imageIndex = 0;
	int skipper = 0;
	
	void Start () 
	{
		image = GetComponent<Image>();
	}
	
	void Update () 
	{
		if (imageIndex < sprites.Length && playForward)
		{
			image.sprite = sprites[imageIndex];
			if(framesToSkip > 0 )
			{
				if (skipper >= framesToSkip)
				{
					skipper = 0;
					imageIndex++;
				}
				else
				{
					skipper++;
				}
			}
			else
			{
				imageIndex++;
			}
		}
		else if (imageIndex > 0 && !playForward)
		{
			image.sprite = sprites[imageIndex-1];
			if(framesToSkip > 0 )
			{
				if (skipper >= framesToSkip)
				{
					skipper = 0;
					imageIndex--;
				}
				else
				{
					skipper++;
				}
			}
			else
			{
				imageIndex--;
			}	
		}
		else if(imageIndex == sprites.Length && loop)
		{

			imageIndex = 0;
		}
		else if (disableOnFinish)
		{
			gameObject.SetActive(false);
		}
	}
}
