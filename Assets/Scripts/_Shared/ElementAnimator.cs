﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public enum Direction
{
	up,
	down,
	left,
	right
}

public class ElementAnimator : MonoBehaviour
{
    [Header("IE Left means from the Left")]
	public Direction enterDirection;
    [Header("IE Left means to the left")]
    public Direction exitDirection;
    public float offDistance = 1000;

    public float speed = 3000;

    public AnimationCurve motionCurve;

    [HideInInspector]
    public bool isAnimating = false;

    public bool changeColor = false;
    public Image image;
    public Text text;
	public TextMeshProUGUI proText;
    public Color offColor;
    public Color onColor;

	private Vector3 onPosition;
	private Vector3 off_EnterPosition;
    private Vector3 off_ExitPosition;

    public bool isOn = false;

	public void Initialize()
	{
		onPosition = transform.localPosition;
		off_EnterPosition = CardinalDestinationPosition(enterDirection, onPosition);
        off_ExitPosition = CardinalDestinationPosition(exitDirection, onPosition);

        if (motionCurve.keys.Length > 0)
        {
            motionCurve.keys[motionCurve.length - 1].value = 1;
        }
        else
        {
            Debug.Log(gameObject.name + " doesn't have motion curve setup");
        }
	}

    public void OnEnable()
    {
        //Off();

        //Initialize();//debug

        //StartCoroutine(AnimateOn(0.5f));//debug
    }

	public IEnumerator AnimateOn()
	{
        isOn = true;
        if (isAnimating)
        {
//            Debug.LogError("Two animations running on "+gameObject.name);
        }
        gameObject.SetActive(true);
        transform.localPosition = off_EnterPosition;
        float startTime = Time.time;
        float duration = motionCurve.keys[motionCurve.length-1].time;
		while (Time.time < startTime + duration + 0.5f)
		{
            isAnimating = true;
            float animationTimePos = Time.time - startTime;
            //Debug.Log("T: " + (t) + "\n Value: " + motionCurve.Evaluate(t));
            Vector3 dir = onPosition - off_EnterPosition;
            transform.localPosition = off_EnterPosition + (dir * motionCurve.Evaluate(animationTimePos));

            if (changeColor)
            {
                float normalizedT = Mathf.InverseLerp(0, duration, animationTimePos);
                if (image != null) image.color = Color.Lerp(offColor, onColor, normalizedT);
                if (text != null) text.color = Color.Lerp(offColor, onColor, normalizedT);
				if (proText != null) proText.color = Color.Lerp(offColor, onColor, normalizedT);
            }
			yield return new WaitForEndOfFrame();
		}
        isAnimating = false;
	}

    public IEnumerator AnimateOn(float delay)
    {
        yield return new WaitForSeconds(delay);
        gameObject.SetActive(true);
        StartCoroutine(AnimateOn());
    }

	public IEnumerator AnimateOff()
	{
        isOn = false;
        StopCoroutine("AnimateOn");
        //transform.localPosition = off_EnterPosition;
        if (isAnimating)
        {
//            Debug.LogError("Two animations running!");
        }
        float startTime = Time.time;
        float duration = motionCurve.keys[motionCurve.length - 1].time;
        Vector3 startPosition = transform.localPosition;
        Vector3 dir = (off_ExitPosition - (startPosition - onPosition) - onPosition);
        while (Time.time < startTime + duration + 0.5f)
        {
            isAnimating = true;
            float animationTimePos = Time.time - startTime;
            //Debug.Log("T: " + (t) + "\n Value: " + motionCurve.Evaluate(t));
            transform.localPosition = startPosition + (dir * motionCurve.Evaluate(animationTimePos));
            if (changeColor)
            {
                float normalizedT = Mathf.InverseLerp(0, duration, animationTimePos);
                if (image != null) image.color = Color.Lerp(onColor, offColor, normalizedT);
                if (text != null) text.color = Color.Lerp(onColor, offColor, normalizedT);
				if (proText != null) proText.color = Color.Lerp(onColor, offColor, normalizedT);
            }
            yield return new WaitForEndOfFrame();
        }
        isAnimating = false;
        gameObject.SetActive(false);
        //StopAllCoroutines();
	}

    public IEnumerator AnimateOff(float delay)
    {
        yield return new WaitForSeconds(delay);
        //sometimes something is asked to be animated off when it was never on,
        //IE when we hit fast forward on piechart before the left panel has had a chance to be introduced
        if (gameObject.activeSelf)
            StartCoroutine(AnimateOff());
    }

    public IEnumerator AnimateOnLeft()
    {
        gameObject.SetActive(true);
        transform.localPosition = CardinalDestinationPosition(Direction.left, onPosition);
        while (Vector3.Distance(transform.localPosition, onPosition) > 1)
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, onPosition, speed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator AnimateOnRight()
    {
        gameObject.SetActive(true);
        transform.localPosition = CardinalDestinationPosition(Direction.right, onPosition);
        while (Vector3.Distance(transform.localPosition, onPosition) > 1)
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, onPosition, speed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator AnimateOffLeft()
    {
        Vector3 offPosition = CardinalDestinationPosition(Direction.left, onPosition);
        while (Vector3.Distance(transform.localPosition, offPosition) > 1)
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, offPosition, speed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
        gameObject.SetActive(false);
    }

    public IEnumerator AnimateOffRight()
    {
        Vector3 offPosition = CardinalDestinationPosition(Direction.right, onPosition);
        while (Vector3.Distance(transform.localPosition, offPosition) > 1)
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, offPosition, speed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
        gameObject.SetActive(false);
    }


	public void On()
	{
        isOn = true;
        gameObject.SetActive(true);
		transform.localPosition = onPosition;
        if (image != null) image.color = onColor;
        if (text != null) text.color = onColor;
	}

	public void Off()
	{
        isOn = false;
        StopAllCoroutines();
        gameObject.SetActive(false);
        transform.localPosition = off_ExitPosition;
        isAnimating = false;

        if (image != null) image.color = offColor;
        if (text != null) text.color = offColor;
	}

    public void AnimateOffStarter()
    {
        if (!isAnimating && gameObject.activeSelf)
        {
            StartCoroutine(AnimateOff());
        }
    }

    private Vector3 CardinalDestinationPosition(Direction direction, Vector3 onPosition)
    {
        Vector3 result = Vector3.zero;
        switch (direction)
        {
            case Direction.up:
                result = onPosition + Vector3.up * offDistance;
                break;

            case Direction.down:
                result = onPosition - Vector3.up * offDistance;
                break;

            case Direction.left:
                result = onPosition - Vector3.right * offDistance;
                break;

            case Direction.right:
                result = onPosition + Vector3.right * offDistance;
                break;
        }
        return result;
    }
}
