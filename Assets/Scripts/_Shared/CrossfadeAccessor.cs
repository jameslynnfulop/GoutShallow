﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CrossfadeAccessor : MonoBehaviour 
{

    Image image;

	void Awake () {
        image = GetComponent<Image>();
	}
	
    public void CrossfadeOn(float duration)
    {
        image = GetComponent<Image>();
        if (image.transform.parent != null)
            image.transform.parent.gameObject.SetActive(true);//if they mask
        image.gameObject.SetActive(true);
        image.color = new Color(1, 1, 1, 0);
        StartCoroutine(FadeAlphaOn(image, duration));
    }
    public void CrossfadeOff(float duration)
    {
        image = GetComponent<Image>();
        if (image.transform.parent != null)
            image.transform.parent.gameObject.SetActive(true);//if they mask
        image.gameObject.SetActive(true);
        image.color = new Color(1, 1, 1, 1);
        StartCoroutine(FadeAlphaOff(image, duration));
    }

    public void SetFullAlpha()
    {
        if (image != null)//null when we come here through main menu before normal path
            image.color = new Color(1, 1, 1, 1);
    }

    IEnumerator FadeAlphaOn(Image image, float duration)
    {
        float startTime = Time.time;
        while (Time.time < startTime + duration + 0.05f)
        {
            float t = Mathf.InverseLerp(0, duration, Time.time - startTime);
            image.color = new Color(1, 1, 1, t);
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator FadeAlphaOff(Image image, float duration)
    {
        float startTime = Time.time;
        while (Time.time < startTime + duration + 0.05f)
        {
            float t = Mathf.InverseLerp(duration, 0, Time.time - startTime);
            image.color = new Color(1, 1, 1, t);
            yield return new WaitForEndOfFrame();
        }
    }

    public void OnDisable()
    {
        StopAllCoroutines();
    }
}
