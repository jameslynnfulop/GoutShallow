﻿using UnityEngine;
using System.Collections;

public class PerlinNoiseMovement : MonoBehaviour
{
    public float samplingScale = 10;
    public float movementScale = 1;
    private Vector2 normalMapOffset;

    public float maxRange = 100;
    private Vector3 startingPosition;

    public float smoothTime = 0.1f;
    public float returnToStartForce = 100;

    public bool usePerlinNoiseSeed;
    public Vector2 perlinNoiseSeed;

    public float rotationSeed;
    private float rotationOffset;
    public float rotationSampleScale;
    public float rotationMovementScale;



    void Start()
    {
        if (usePerlinNoiseSeed)
        {
            rotationOffset = rotationSeed;
            normalMapOffset = perlinNoiseSeed;
        }
        else
        {
            rotationOffset = Random.Range(0, 10);
            normalMapOffset = new Vector2(Random.Range(0, 10), Random.Range(0, 10));
        }

        startingPosition = transform.localPosition;
    }

    void OnEnable()
    {
        if (startingPosition != Vector3.zero)
            transform.localPosition = startingPosition;

    }

    private Vector3 velocity;
    void Update()
    {
        float samplePoint = Time.time * samplingScale;
        Vector3 perlinPosition =
        new Vector3(Mathf.Lerp(-1, 1, Mathf.PerlinNoise(normalMapOffset.x + samplePoint, normalMapOffset.x + samplePoint)),
                    Mathf.Lerp(-1, 1, Mathf.PerlinNoise(normalMapOffset.y + samplePoint, normalMapOffset.y + samplePoint)),
        0) * movementScale;

        perlinPosition += transform.localPosition;
        Vector3 smoothDampPos = Vector3.SmoothDamp(perlinPosition, startingPosition, ref velocity, smoothTime, returnToStartForce);

        transform.localPosition = smoothDampPos;

        float rotationSamplePoint = Time.time * rotationSampleScale;
        transform.localEulerAngles = new Vector3
        (
            transform.localEulerAngles.x,
            transform.localEulerAngles.y,
            Mathf.Lerp(0,360,Mathf.PerlinNoise(rotationOffset + rotationSamplePoint, 0))* rotationMovementScale
        );
    }
}
