#define Analytics

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FastFactsController : MonoBehaviour
{
#if Analytics
    public GoogleAnalyticsV3 googleAnalytics;
#endif
	public ScreenManager screenManager;

	public GameObject fastFactsPanel;
	public GameObject pageMaster;
	private Animator fastFactsAnimator;

	private Button button;
	private bool isOn = false;

	public void Toggle()
	{
		isOn = !isOn;
		fastFactsAnimator.SetBool("isOn", isOn);
	}

	private void Start()
	{
		fastFactsAnimator = fastFactsPanel.GetComponent<Animator>();
	}

	private void OnEnable()
	{
#if Analytics
		googleAnalytics.LogScreen(screenManager.GetCurrentlyOpenPanel().gameObject.name + " Fast Facts");
#endif
	}
}
