﻿using UnityEngine;
using System.Collections;

public class Highlight : MonoBehaviour
{

	public GameObject blue;
	public GameObject orange;
	public GameObject wine;


	public void Blue()
	{
		blue.GetComponent<Flipbook>().loop = true;
		blue.GetComponent<Flipbook>().imageIndex = 0;
		blue.SetActive(  true);
		orange.SetActive(false);
		wine.SetActive(  false);
	}

	public void Orange()
	{
		orange.GetComponent<Flipbook>().loop = true;
		blue.SetActive(  false);
		orange.GetComponent<Flipbook>().imageIndex = 0;
		orange.SetActive(true);
		blue.SetActive(  false);
	}

	public void Wine()
	{
		wine.GetComponent<Flipbook>().loop = true;
		blue.SetActive(  false);
		orange.SetActive(false);
		wine.GetComponent<Flipbook>().imageIndex = 0;
		wine.SetActive(  true);
	}

	public void Stop()
	{
		blue.GetComponent<Flipbook>().loop = false;
		orange.GetComponent<Flipbook>().loop = false;
		wine.GetComponent<Flipbook>().loop = false;
	}
}
