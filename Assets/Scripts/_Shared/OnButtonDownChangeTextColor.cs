﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class OnButtonDownChangeTextColor : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler {

    public Text text;

    public Color selectedColor;
    public Color normalColor;
	
	void Update () 
    {
	    if (isDown && isHovered)
        {
            text.color = selectedColor;
        }
        else
        {
            text.color = normalColor;
        }
	}

    bool isHovered;
    bool isDown;
    public void OnPointerUp(PointerEventData eventData)
    {
        isDown = false;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isDown = true;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        isHovered = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isHovered = false;
    }
}
