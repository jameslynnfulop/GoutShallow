﻿using UnityEngine;
using System.Collections;
using MMT;

public class LoopMovieSection : MonoBehaviour 
{

	public double loopStartPos;
	public double loopEndPos;

	public MobileMovieTexture startMovie;
	public MobileMovieTexture loopMovie;
	public MobileMovieTexture endMovie;

	public bool pause = false;

	public double playPosition;
	public bool doLoop;
	// Use this for initialization
	void Start () 
	{

		loopMovie.enabled = false;
		endMovie.enabled = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!startMovie.IsPlaying && startMovie.PlayPosition > 0)
		{
			startMovie.enabled = false;
			loopMovie.enabled = true;
		}

		if (!loopMovie.IsPlaying && loopMovie.PlayPosition > 0)
		{
			loopMovie.enabled = false;
			endMovie.enabled = true;
		}
	}
}
