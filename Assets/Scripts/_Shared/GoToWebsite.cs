﻿using UnityEngine;
using System.Collections;

public class GoToWebsite : MonoBehaviour
{
	public string webURL = "http://www.therealgout.com/";
	public void ToSite()
	{
		Application.OpenURL(webURL);
	}
}
