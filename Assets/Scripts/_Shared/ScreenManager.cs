//#define Analytics

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;

public class ScreenManager : MonoBehaviour
{

    //Screen to open automatically at the start of the Scene
    public Animator initiallyOpen;
#if Analytics
    public GoogleAnalyticsV4 googleAnalytics;
#endif
    //Currently Open Screen
    private Animator m_Open;

    private Animator overlayPanel;

    //Hash of the parameter we use to control the transitions.
    private int m_OpenParameterId;

    //The GameObject Selected before we opened the current Screen.
    //Used when closing a Screen, so we can go back to the button that opened it.
    private GameObject m_PreviouslySelected;

    //Animator State and Transition names we need to check against.
    const string k_OpenTransitionName = "isOn";
    const string k_ClosedStateName = "Off";

    public float slideSpeed = 1;

    //public MotionBlur motionBlurComponent;

    public float delayWhenOpeningDifferentOverlayPanel = 0.5f;

    DockBounceAccessor dockBounceAccessor;

    public void OnEnable()
    {
        //We cache the Hash to the "Open" Parameter, so we can feed to Animator.SetBool.
        m_OpenParameterId = Animator.StringToHash (k_OpenTransitionName);

        //If set, open the initial Screen now.
        if (initiallyOpen == null)
            return;
        OpenPanel(initiallyOpen);
        dockBounceAccessor = GetComponent<DockBounceAccessor>();
    }

    bool isInit = true;
    //Closes the currently open panel and opens the provided one.
    //It also takes care of handling the navigation, setting the new Selected element.
    public void OpenPanel (Animator anim)
    {
    	CloseOverlayPanel();

        if (m_Open == anim)
        {
          fakeBackground = anim.GetComponent<BackButtonModule>().fakeBackground;
          Reset();
          return;
        }


        //Activate the new Screen hierarchy so we can animate it.
        anim.gameObject.SetActive(true);
        //Save the currently selected button that was used to open this Screen. (CloseCurrent will modify it)
        var newPreviouslySelected = EventSystem.current.currentSelectedGameObject;
        //Move the Screen to front.
        //anim.transform.SetAsLastSibling();

        CloseCurrent(true);

        m_PreviouslySelected = newPreviouslySelected;

        //Set the new Screen as then open one.
        m_Open = anim;
        //Start the open animation
        StartCoroutine(OpenSlide(m_Open.transform, slideSpeed, isInit));

        //Set an element in the new screen as the new Selected one.
        GameObject go = FindFirstEnabledSelectable(anim.gameObject);
        SetSelected(go);
#if Analytics
        googleAnalytics.LogScreen(anim.gameObject.name);
#endif
    }

	public void OpenPanelNoSlide (Animator anim)
    {
    	CloseOverlayPanel();

        if (m_Open == anim)
            return;

        m_Open.SetBool("toNext", true);
        //Activate the new Screen hierarchy so we can animate it.
        anim.gameObject.SetActive(true);
        //Save the currently selected button that was used to open this Screen. (CloseCurrent will modify it)
        var newPreviouslySelected = EventSystem.current.currentSelectedGameObject;
        //Move the Screen to front.
        //anim.transform.SetAsLastSibling();

        CloseCurrent(false);


        m_PreviouslySelected = newPreviouslySelected;

        //Set the new Screen as then open one.
        m_Open = anim;
        m_Open.SetBool("toNext", true);

		m_Open.SetBool(m_OpenParameterId, true);

        //Set an element in the new screen as the new Selected one.
        GameObject go = FindFirstEnabledSelectable(anim.gameObject);
        SetSelected(go);
//#if Analytics
//        googleAnalytics.LogScreen(anim.gameObject.name);
//#endif
    }

        //Closes the currently open panel and opens the provided one.
    //It also takes care of handling the navigation, setting the new Selected element.
    public void OpenOverlayPanel (Animator anim)
    {
//#if Analytics
//        googleAnalytics.LogScreen(anim.gameObject.name);
//#endif
        StartCoroutine(OpenOverlayPanelCoroutine(anim));
    }

    IEnumerator OpenOverlayPanelCoroutine(Animator anim)
    {
        if (overlayPanel == anim)
            yield return null;

        if (overlayPanel != null)
        {
            CloseOverlayPanel();
            yield return new WaitForSeconds(delayWhenOpeningDifferentOverlayPanel);
        }
        else
        {
            CloseOverlayPanel();
        }

        //Activate the new Screen hierarchy so we can animate it.
        anim.gameObject.SetActive(true);
        //Move the Screen to front.
        //anim.transform.SetAsLastSibling();

        //Set the new Screen as then open one.
        overlayPanel = anim;
        //Start the open animation
        overlayPanel.SetBool(m_OpenParameterId, true);

        transform.gameObject.SetActive(true);

        //Set an element in the new screen as the new Selected one.
        GameObject go = FindFirstEnabledSelectable(anim.gameObject);
        SetSelected(go);
    }

    //returns true if there was something to turn off
    public void CloseOverlayPanel()
    {
    	if (overlayPanel == null)
            return;

        //Start the close animation.
        overlayPanel.SetBool(m_OpenParameterId, false);

        //Start Coroutine to disable the hierarchy when closing animation finishes.
        StartCoroutine(DisablePanelDeleyed(overlayPanel));

        //No screen open.
        overlayPanel = null;
    }

    //Finds the first Selectable element in the providade hierarchy.
    static GameObject FindFirstEnabledSelectable (GameObject gameObject)
    {
        GameObject go = null;
        var selectables = gameObject.GetComponentsInChildren<Selectable> (true);
        foreach (var selectable in selectables) {
            if (selectable.IsActive () && selectable.IsInteractable ()) {
                go = selectable.gameObject;
                break;
            }
        }
        return go;
    }

    //Closes the currently open Screen
    //It also takes care of navigation.
    //Reverting selection to the Selectable used before opening the current screen.
    public void CloseCurrent(bool doSlide)
    {
        if (m_Open == null)
            return;

        //Start the close animation.
		if (doSlide)
        {
			StartCoroutine(CloseSlide(m_Open.transform, slideSpeed));
        }
        else
        {
        	//Start Coroutine to disable the hierarchy when closing animation finishes.
            m_Open.SetBool(m_OpenParameterId, false);

        	StartCoroutine(DisablePanelDeleyed(m_Open));
        }

        //Reverting selection to the Selectable used before opening the current screen.
        SetSelected(m_PreviouslySelected);


        //No screen open.
        m_Open = null;
        dockBounceAccessor.TurnOffDockBounce();

    }

    //Coroutine that will detect when the Closing animation is finished and it will deactivate the
    //hierarchy.
    IEnumerator DisablePanelDeleyed(Animator anim)
    {
    	anim.logWarnings = false;
        bool closedStateReached = false;
        bool wantToClose = true;
        while (!closedStateReached && wantToClose)
        {
            if (anim.GetCurrentAnimatorStateInfo(0).IsName("Nice To Off") || anim.GetCurrentAnimatorStateInfo(0).IsName("Off"))
            {
                if (anim.IsInTransition(0) || anim.GetCurrentAnimatorStateInfo(0).IsName("Off"))
                {
                    closedStateReached = true;
                }
            }
            wantToClose = !anim.GetBool(m_OpenParameterId);
            yield return new WaitForEndOfFrame();
        }

        if (wantToClose)
        {
        	anim.SetBool("toNext", false);
            anim.gameObject.SetActive(false);
        }
    }

    //Make the provided GameObject selected
    //When using the mouse/touch we actually want to set it as the previously selected and
    //set nothing as selected for now.
    private void SetSelected(GameObject go)
    {
        //Select the GameObject.
        EventSystem.current.SetSelectedGameObject(go);



        //Since we are using a pointer device, we don't want anything selected.
        //But if the user switches to the keyboard, we want to start the navigation from the provided game object.
        //So here we set the current Selected to null, so the provided gameObject becomes the Last Selected in the EventSystem.
        EventSystem.current.SetSelectedGameObject(null);
    }

    IEnumerator CloseSlide (Transform transform,  float duration)
    {
        //motionBlurComponent.enabled = true;
        Vector3 startPosition = Vector3.zero;
        Vector3 endPosition = new Vector3(-1920,0,0);
        float startTime = Time.time;

        transform.localPosition = startPosition;
        while (transform.localPosition != endPosition)
        {
            transform.localPosition = Vector3.Lerp(startPosition, endPosition, (Time.time - startTime) / duration);
            yield return new WaitForEndOfFrame();
        }
        transform.GetComponent<Animator>().SetBool(m_OpenParameterId, false);
        if (transform.GetComponent<PageAnimator>() != null)
            transform.GetComponent<PageAnimator>().AllOff();
        transform.localPosition = new Vector3(0,0,0);
        transform.gameObject.SetActive(false);
        //motionBlurComponent.enabled = false;
    }

    IEnumerator OpenSlide (Transform transform,  float duration, bool isInit)
    {
        transform.GetComponent<Animator>().SetBool(m_OpenParameterId, true);
        Vector3 startPosition = new Vector3(1920,0,0);
        Vector3 endPosition = Vector3.zero;
        float startTime = Time.time;

        transform.gameObject.SetActive(true);
        if (isInit)
        {
            this.isInit = false;
            yield return null;
        }
        else
        {
            transform.localPosition = startPosition;
            while (transform.localPosition != endPosition)
            {
                transform.localPosition = Vector3.Lerp(startPosition, endPosition, (Time.time - startTime) / duration);
                yield return new WaitForEndOfFrame();
            }
        }

    }

    public Animator GetCurrentlyOpenPanel()
    {
        return m_Open;
    }

    private Animator lastPanel;
    public Animator empty;
    public Image fakeBackground;
    public void Reset()
    {
        lastPanel = m_Open;

        //Invoke("TurnOnNextFrame", 0.5f);
        StartCoroutine(fadeOnImage(fakeBackground));
    }

    public IEnumerator fadeOnImage(Image image)
    {
        image.gameObject.SetActive(true);
      float startTime = Time.time;
      while (Time.time < startTime + 0.1f)
      {
        image.color = new Color(1,1,1,Mathf.InverseLerp(startTime, startTime + 0.1f, Time.time));
        yield return new WaitForEndOfFrame();
      }
      image.color = new Color(1,1,1,1);
      OpenPanel(empty);
      yield return new WaitForSeconds(0.5f);
      OpenPanel(lastPanel);
      yield return new WaitForSeconds(0.2f);
      float startTimeTwo = Time.time;
      while (Time.time < startTimeTwo + 0.1f)
      {
        image.color = new Color(1,1,1,Mathf.InverseLerp(startTimeTwo + 0.1f, startTimeTwo, Time.time));
        yield return new WaitForEndOfFrame();
      }
      image.color = new Color(1,1,1,0);
      image.gameObject.SetActive(false);
    }
}
