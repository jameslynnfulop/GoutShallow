﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScrubbableFlipbook : MonoBehaviour
{

	public Sprite[] sprites;
	public int framesToSkip = 0;

	Image image;

	int skipper = 0;

	[Range(0,1)]
	public float targetPercent;

	private int targetFrame;
	[SerializeField]
	private int currentFrame;


	void Start ()
	{
		image = GetComponent<Image>();

	}

    void OnEnable()
    {
        image = GetComponent<Image>();
        image.color = new Color(1, 1, 1, 0);
        image.sprite = sprites[0];
        targetPercent = 0;
        targetFrame = 0;
        currentFrame = 0;
    }

	void Update ()
	{
        if (currentFrame > 1)
        {
            image.color = new Color(1, 1, 1, 1);
        }
        else
        {
        	image.color = new Color(1, 1, 1, 0);
        }

		targetFrame = Mathf.RoundToInt((sprites.Length-1) * targetPercent);
		if (currentFrame < targetFrame)
		{
			image.sprite = sprites[currentFrame];
			if(framesToSkip > 0 )
			{
				if (skipper >= framesToSkip)
				{
					skipper = 0;
					currentFrame++;
				}
				else
				{
					skipper++;
				}
			}
			else
			{
				currentFrame++;
			}
		}
		else if (currentFrame > targetFrame)
		{
			image.sprite = sprites[currentFrame-1];
			if(framesToSkip > 0 )
			{
				if (skipper >= framesToSkip)
				{
					skipper = 0;
					currentFrame--;
				}
				else
				{
					skipper++;
				}
			}
			else
			{
				currentFrame--;
			}
		}
	}
}
