﻿using UnityEngine;
using System.Collections;

public class SendEmail : MonoBehaviour
{
	public void SendMail ()
	{
		string email = "placeholder@ironwoodpharma.com";
		string subject = MyEscapeURL("Inside Gout: App Support");
		string body = MyEscapeURL("");

		Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
	}

	string MyEscapeURL (string url)
	{
		return WWW.EscapeURL(url).Replace("+","%20");
	}
}
