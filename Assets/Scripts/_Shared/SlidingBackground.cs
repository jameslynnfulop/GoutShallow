﻿using UnityEngine;
using System.Collections;

public class SlidingBackground : MonoBehaviour 
{

    float minAngleX = -0.5f;
    float maxAngleX = 0.5f;

    float maxAngleY = 0;
    //float yCenter = -0.2f;
    float minAngleY = -0.4f;
    
    //public float maxSpeed = 100;

    public float minX;
    public float maxX;
    public float minY;
    public float maxY;

	[Range(-1,1)]
	public float fakeXValue;
	[Range(-1,1)]
	public float fakeYValue;
	
    private Vector3 startingGravity;

    //[HideInInspector]
    public Vector3 baseLocation;

    Vector3 startingPosition;
	// Use this for initialization
	void Start () 
    {
        startingPosition = transform.localPosition;
        baseLocation = startingPosition;
        Input.gyro.enabled = true;
    }

    void OnEnable()
    {        
        startingGravity = Input.gyro.gravity;
        minAngleX = startingGravity.x - 0.5f;
        maxAngleX = startingGravity.x + 0.5f;


        minAngleY = startingGravity.y - 0.5f;
        maxAngleY = startingGravity.y + 0.5f;}
	
    void OnDisable()
    {
        baseLocation = startingPosition;
        transform.localPosition = startingPosition;

    }
	// Update is called once per frame
	void Update () 
    {
        //
        // horizontal
        //
        Vector3 gravity = Input.gyro.gravity;
        gravity.x += fakeXValue;
        gravity.y += fakeYValue;

        if (gravity.x < startingGravity.x)
        {
        	transform.localPosition = new Vector3(Mathf.Lerp(baseLocation.x, baseLocation.x + minX,Mathf.InverseLerp(startingGravity.x, minAngleX, gravity.x)), 
        										transform.localPosition.y, 
        										transform.localPosition.z);
        }
        else if (gravity.x >= startingGravity.x)
        {

        	transform.localPosition = new Vector3(Mathf.Lerp(baseLocation.x, baseLocation.x + maxX,Mathf.InverseLerp(startingGravity.x, maxAngleX, gravity.x)), 
        										transform.localPosition.x, 
        										transform.localPosition.z);
        }

		//
        // vertical
        //
        if (gravity.y < startingGravity.y)
        {
        	transform.localPosition = new Vector3(transform.localPosition.x, 
        										Mathf.Lerp(baseLocation.y, baseLocation.y + minY,Mathf.InverseLerp(startingGravity.y, minAngleY, gravity.y)), 
        										transform.localPosition.z);
        }
        else if (gravity.y >= startingGravity.y)
        {

        	transform.localPosition = new Vector3(transform.localPosition.x, 
        										Mathf.Lerp(baseLocation.y, baseLocation.y +maxX, Mathf.InverseLerp(startingGravity.y, maxAngleY, gravity.y)), 
        										transform.localPosition.z);
        }
	}


}
