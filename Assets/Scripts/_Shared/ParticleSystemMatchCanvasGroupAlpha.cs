﻿using UnityEngine;
using System.Collections;

public class ParticleSystemMatchCanvasGroupAlpha : MonoBehaviour 
{
	public CanvasGroup canvasGroup;

	Material mat;
	// Use this for initialization
	void Start () 
	{
		//particleSystem = GetComponent<ParticleSystem>();
		mat = GetComponent<Renderer>().material;
	}
	
	// Update is called once per frame
	void Update () 
	{
		mat.SetFloat("_Fader", canvasGroup.alpha);
	}
}
