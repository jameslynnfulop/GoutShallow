﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public class ReferencesController : MonoBehaviour
{
	public TextMeshProUGUI body;
	public Text title;

	void OnEnable()
	{
		if (body.text.Contains("\n"))
		{
			if (body.text.IndexOf('\n') != body.text.LastIndexOf('\n'))
			{
				title.text = "References";
			}
			else
			{
				title.text = "Reference";
			}
		}
	}
}
