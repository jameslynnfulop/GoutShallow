﻿using UnityEngine;
using System.Collections;

public class DockBounceAccessor : MonoBehaviour 
{
    public float delay = 5;
    public Animator sharedMenuController;
	
	void Start () 
    {
	
	}
	
	public void TurnOnDockBounce()
    {
        Invoke("ActuallyTurnOn", delay);
    }

    private void ActuallyTurnOn()
    {
        sharedMenuController.SetBool("isDockBouncing", true);
    }

    public void TurnOffDockBounce()
    {
        sharedMenuController.SetBool("isDockBouncing", false);
    }
}
