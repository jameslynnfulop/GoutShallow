﻿using UnityEngine;
using System.Collections;

public class LockWorldScale : MonoBehaviour
{
	public Transform transformToCounter;
	public Transform transformToCounterTwo;

	void Update ()
	{
		if (transformToCounterTwo != null)
		{
		transform.localScale = new Vector3(1 / (transformToCounter.localScale.x + transformToCounterTwo.localScale.x),
										   1 / (transformToCounter.localScale.y + transformToCounterTwo.localScale.y),
										   1 / (transformToCounter.localScale.z + transformToCounterTwo.localScale.z));
		}
		else
		{
					transform.localScale = new Vector3(1 / (transformToCounter.localScale.x),
										   1 / (transformToCounter.localScale.y),
										   1 / (transformToCounter.localScale.z));
		}

	}
}
