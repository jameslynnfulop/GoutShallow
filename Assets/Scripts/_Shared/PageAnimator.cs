﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Subpage
{
    public GameObject topLevelPage;

	[HideInInspector]
    public ElementAnimator[] elements;

    public void Initialize()
    {
        if (topLevelPage.transform.childCount > 0)
        {
            elements = topLevelPage.GetComponentsInChildren<ElementAnimator>();
        }
        else if (topLevelPage.GetComponent<ElementAnimator>() != null)
        {
            elements = new ElementAnimator[1];
            elements[0] = topLevelPage.GetComponent<ElementAnimator>();
        }
        else
        {
            Debug.Log(topLevelPage.name + " doesn't have any element animators");
        }

        foreach (ElementAnimator element in elements)
        {
            element.Initialize();
        }
    }
}

public class PageAnimator : MonoBehaviour
{
	public Subpage[] pages;

    public bool disableOnStart = true;

    void Awake()
    {
        foreach (Subpage page in pages)
        {
            page.Initialize();
        }
    }

    void OnEnable()
    {

        //only true for main menu
        if (disableOnStart)
            AllOff();
    }

    public void OnSubpage(string name)
    {
        for (int i = 0; i < pages.Length; i++)
        {
            if (pages[i].topLevelPage.name == name)
            {
                On(pages[i]);
                break;
            }
        }
    }

    public void OffSubpage(string name)
    {
        for (int i = 0; i < pages.Length; i++)
        {
            if (pages[i].topLevelPage.name == name)
            {
                Off(pages[i]);
                break;
            }
        }
    }

	public void AnimateOnSubpage(string name)
	{
        for (int i = 0; i < pages.Length; i++ )
        {
            if (pages[i].topLevelPage.name == name)
            {
                AnimateOn(pages[i]);
                break;
            }
        }
	}

	public void AnimateOffSubpage(string name)
	{
        for (int i = 0; i < pages.Length; i++)
        {
            if (pages[i].topLevelPage.name == name)
            {
                AnimateOff(pages[i]);
                break;
            }
            if (i == pages.Length -1)
            {
                Debug.Log(name + " not found in PageAnimator");
            }
        }

	}

    public void AnimateOnSubpage(string name, float delay)
    {
        for (int i = 0; i < pages.Length; i++)
        {
            if (pages[i].topLevelPage.name == name)
            {
                AnimateOn(pages[i], delay);
                break;
            }
        }
    }

    public void AnimateOffSubpage(string name, float delay)
    {
        for (int i = 0; i < pages.Length; i++)
        {
            if (pages[i].topLevelPage.name == name)
            {
                AnimateOff(pages[i], delay);
                break;
            }
        }
    }

    public void AllOn()
    {
        foreach (Subpage page in pages)
        {
            foreach (ElementAnimator element in page.elements)
                element.On();
        }
    }

	public void AllOff()
	{
		foreach (Subpage page in pages)
        {
			foreach (ElementAnimator element in page.elements)
				element.Off();
        }
	}

	public void AnimateAllOff()
	{
		foreach (Subpage page in pages)
			foreach (ElementAnimator element in page.elements)
				StartCoroutine(element.AnimateOff());
	}

	private void On(Subpage page)
	{
		foreach (ElementAnimator element in page.elements)
			element.On();
	}

    private void Off(Subpage page)
	{

		foreach (ElementAnimator element in page.elements)
			element.Off();
	}

    private void AnimateOn(Subpage page)
	{

		foreach (ElementAnimator element in page.elements)
			StartCoroutine(element.AnimateOn());
	}

    private void AnimateOff(Subpage page)
	{
		foreach (ElementAnimator element in page.elements)
			StartCoroutine(element.AnimateOff());
	}

    private void AnimateOn(Subpage page, float delay)
    {

        foreach (ElementAnimator element in page.elements)
            StartCoroutine(element.AnimateOn(delay));
    }

    private void AnimateOff(Subpage page, float delay)
    {
        foreach (ElementAnimator element in page.elements)
            StartCoroutine(element.AnimateOff(delay));
    }
}
