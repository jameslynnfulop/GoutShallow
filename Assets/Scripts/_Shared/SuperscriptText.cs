﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

[RequireComponent(typeof(Text))]
public class SuperscriptText : MonoBehaviour 
{
	Text text;
	string currentString;

	void OnEnable () 
	{
		PlaceSuperscripts();
	}
	
	void PlaceSuperscripts()
	{

		text = GetComponent<Text>();
		String str = text.text;
		str = str.Replace("{super1}", "\u00B9");
		str = str.Replace("{super2}", "\u00B2");
		str = str.Replace("{super3}", "\u00B3");
		str = str.Replace("{super4}", "\u2074");
		str = str.Replace("{super5}", "\u2075");
		str = str.Replace("{super6}", "\u2076");
		str = str.Replace("{super7}", "\u2077");
		str = str.Replace("{super8}", "\u2078");
		str = str.Replace("{super9}", "\u2079");
		str = str.Replace("{super,}", "'");
        str = str.Replace("{super-}", "\u207B");
		text.text = str;
		currentString = text.text;	
	}

	void Update () 
	{
		if (currentString != text.text)
		{
			PlaceSuperscripts();
		}
	}
}
