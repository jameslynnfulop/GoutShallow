#define Analytics

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public class QuizController : MonoBehaviour
{
	[System.Serializable]
	public struct QuizQuestion
	{
		public AnswerResponsePair[] pairs;
		public Animator panel;
		public TextAsset reference;
        public GameObject resultsPanelSetup;
        public TextMeshProUGUI resultsPanelText;
	}

	[System.Serializable]
	public struct AnswerResponsePair
	{
		public Button answer;
		public TextAsset response;
		public bool correct;

	}
#if Analytics
	public GoogleAnalyticsV3 googleAnalytics;
#endif
	public ScreenManager manager;

	public QuizQuestion[] quizQuestions;


	public Animator resultPanel;
    public TextMeshProUGUI resultNextButtonText;

	public Animator finalPanel;
	public SharedUIElementsController uiController;
	public TextMeshProUGUI referencesPanelText;

	private int currentQuestion = 0;
	private Animator currentPanel;

	//public Transform backgroundFluid;
	private float startHeight;
	public float endHeight;
	private float fluidMoveDelta;


    private float currentTargetHeight;
    public float backgroundMoveSpeed = 50;

	public SlidingBackground backgroundFluid;

    public AudioSource correctSound;
    public AudioSource incorrectSound;

    public GameObject[] resultsPanelSetups;

	public BackButtonCallback backButtonCallback;

    void Awake()
    {
        //startHeight = backgroundFluid.GetComponent<RectTransform>().rect.yMin;
        startHeight = backgroundFluid.transform.localPosition.y;
        endHeight = -startHeight - 200;
    }

	void Start()
	{
		currentPanel = quizQuestions[0].panel;
        currentPanel.SetBool("isOn", true);
		fluidMoveDelta = (endHeight - startHeight) / (quizQuestions.Length * 2);
		//StartCoroutine(backgroundFluid.GetComponent<ElementAnimator>().AnimateOn());
	}

    void OnEnable()
    {
        currentQuestion = 0;
        currentPanel = quizQuestions[0].panel;
        backgroundFluid.baseLocation.y = startHeight;
        uiController.ToMenu();
        currentTargetHeight = startHeight;
        resultNextButtonText.text = "Next";
       // backgroundFluid.gameObject.SetActive(true);
       // Invoke("EnableBackgroundFluid", 0.1f);
    }
    /*
    void EnableBackgroundFluid()
    {

        backgroundFluid.GetComponent<MobileMovieTexture>().Play();
    }

    void OnDisable()
    {
        backgroundFluid.gameObject.SetActive(false);
    }*/

	void Update()
    {
        backgroundFluid.baseLocation.y = Mathf.MoveTowards(backgroundFluid.baseLocation.y,
															currentTargetHeight,
															Time.deltaTime * backgroundMoveSpeed);
        //weird
        if(currentQuestion == 0 && !resultPanel.GetBool("isOn"))
            quizQuestions[0].panel.SetBool("isOn", true);
    }


	void DisplayResult(AnswerResponsePair pair, TextAsset reference)
	{
		SwitchPanel(resultPanel);
		//resultContent.text = pair.response.text;
		currentTargetHeight += fluidMoveDelta;
		referencesPanelText.text = reference.text;
	}

	public void NextQuestion()
	{
		currentQuestion++;
		if (currentQuestion < quizQuestions.Length)
		{
			SwitchPanel(quizQuestions[currentQuestion].panel);
		}
		else
		{
			manager.OpenPanel(finalPanel);
			uiController.ToMenu();
		}

        currentTargetHeight += fluidMoveDelta;
	}

	void SwitchPanel(Animator animator)
	{
		currentPanel.SetBool("isOn", false);
		animator.SetBool("isOn", true);

		currentPanel = animator;
	}

	public void Choice(Button button)
	{
		uiController.ToMenu();
		backButtonCallback.ResetModuleOnBackButtonClick();
		for (int i = 0; i < quizQuestions[currentQuestion].pairs.Length; i++)
		{
			if (quizQuestions[currentQuestion].pairs[i].answer == button)
			{
				DisplayResult(quizQuestions[currentQuestion].pairs[i], quizQuestions[currentQuestion].reference);
                quizQuestions[currentQuestion].resultsPanelText.text = quizQuestions[currentQuestion].pairs[i].response.text;
                foreach (GameObject setups in resultsPanelSetups)
                    setups.SetActive(false);

                quizQuestions[currentQuestion].resultsPanelSetup.SetActive(true);

                if (currentQuestion > 0)

                if (currentQuestion == quizQuestions.Length - 1)
                {
                    resultNextButtonText.text = "Finish";
                }

                if (quizQuestions[currentQuestion].pairs[i].correct)
                {
                    correctSound.Play();
                }
                else
                {
                    incorrectSound.Play();
                }
#if Analytics
				EventHitBuilder hit = new EventHitBuilder();
				hit.SetEventCategory("Quiz Answer "+currentQuestion);
				hit.SetEventAction("Answer "+button.gameObject.name);
				string correct = (quizQuestions[currentQuestion].pairs[i].correct) ? "Correct" : "Incorrect";
				hit.SetEventLabel(correct);
				hit.SetEventValue(0);
				if (googleAnalytics)googleAnalytics.LogEvent(hit);
#endif
			}
		}
	}

    /*IEnumerator SlideOverTarget()
	{
		backgroundFluid.GetComponent<ElementAnimator>().AnimateOn();
	}*/
}
