﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public class ScrollbarReferences : MonoBehaviour
{
	public Text text;
	public TextMeshProUGUI proText;

	public RectTransform scrollMaskRect;
	public GameObject scrollbar;

	private Scrollbar scrollbarComponent;
	void Awake ()
	{
		scrollbarComponent = scrollbar.GetComponent<Scrollbar>();
	}

	void OnEnable()
	{
		scrollbarComponent.value = 1;
	}

	void Update ()
	{
		if (text != null)
		{
			if (text.preferredHeight > scrollMaskRect.rect.height)
			{
				scrollbar.SetActive(true);
			}
			else
			{
				scrollbar.SetActive(false);
			}
		}

		if (proText != null)
		{
			if (proText.preferredHeight > scrollMaskRect.rect.height)
			{
				scrollbar.SetActive(true);
			}
			else
			{
				scrollbar.SetActive(false);
			}
		}

	}
}
