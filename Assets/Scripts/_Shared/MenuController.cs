﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuController : MonoBehaviour 
{
	public GameObject menuPanel;
	private Animator menuAnimator;

	private Button button;
	private bool isOn = false;


	// Use this for initialization
	void Start () 
	{
		
		menuAnimator = menuPanel.GetComponent<Animator>();
	}

	public void Toggle()
	{
		isOn = !isOn;
		menuAnimator.SetBool("isOn", isOn);
	}
	
}

