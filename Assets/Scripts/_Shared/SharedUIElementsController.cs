﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SharedUIElementsController : MonoBehaviour 
{
	private Animator animator;

	public Scrollbar scrollBar;
	void Awake()
	{
		animator = GetComponent<Animator>();
	}

	public void ToAll()
	{
        AnimatorStateInfo info = animator.GetCurrentAnimatorStateInfo(0);
        if (info.IsName("All"))
            return;
		animator.SetTrigger("All");
		scrollBar.value = 1;
        animator.SetBool("CanTransition", false);
        Invoke("CanTransition", 0.3f);
	}

	public void ToMenuAndReferences()
	{
        AnimatorStateInfo info = animator.GetCurrentAnimatorStateInfo(0);
        if (info.IsName("MenuReferences"))
            return;
		animator.SetTrigger("MenuReferences");
		scrollBar.value = 1;
        animator.SetBool("CanTransition", false);
        Invoke("CanTransition", 0.3f);
	}

	public void ToOff()
	{
        if (animator == null)
            animator = GetComponent<Animator>();
        AnimatorStateInfo info = animator.GetCurrentAnimatorStateInfo(0);
        if (info.IsName("Off"))
            return;
		animator.SetTrigger("Off");
		scrollBar.value = 1;
        animator.SetBool("CanTransition", false);
        Invoke("CanTransition", 0.3f);
	}

	public void ToMenu()
	{
        if (animator == null)
            animator = GetComponent<Animator>();
        AnimatorStateInfo info = animator.GetCurrentAnimatorStateInfo(0);
        if (info.IsName("Menu"))
            return;
		animator.SetTrigger("Menu");
		scrollBar.value = 1;
        animator.SetBool("CanTransition", false);
        Invoke("CanTransition", 0.3f);
	}

    void CanTransition()
    {
        animator.SetBool("CanTransition", true);
    }
}
