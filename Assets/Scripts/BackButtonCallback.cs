﻿using UnityEngine;

using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Collections;
using UnityEngine.UI;

public enum OnClickOptions
{
    ResetModule,
    BackMajorModule
}

public class BackButtonCallback : MonoBehaviour, IPointerClickHandler {

    public UnityEvent otherButtonEvent;
    public UnityEvent reset;

    public OnClickOptions clickBehaviour = OnClickOptions.ResetModule;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

    private float lastHitTime = 0;
    public void OnPointerClick(PointerEventData eventData)
    {
        if (Time.time < lastHitTime + 0.6f)
                return;

        lastHitTime = Time.time;
        switch (clickBehaviour)
        {
            case OnClickOptions.ResetModule:
                reset.Invoke();
                break;

            case OnClickOptions.BackMajorModule:
                otherButtonEvent.Invoke();
                break;
        }
    }

    public void ResetModuleOnBackButtonClick()
    {
        clickBehaviour = OnClickOptions.ResetModule;
    }

    public void BackOneModuleOnBackButtonClick()
    {
        clickBehaviour = OnClickOptions.BackMajorModule;
    }
}
