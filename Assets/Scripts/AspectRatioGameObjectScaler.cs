﻿using UnityEngine;
using System.Collections;

public class AspectRatioGameObjectScaler : MonoBehaviour
{

    public RectTransform topUICanvas;
    public float maxScale = 1;
    public float minScale = 0.5f;
    public float maxScaleAtRatio = 1.33f;
    public float minScaleAtRatio = 1.77f;

    private float aspectRatio;


    //public bool useScalingPositioning = false;
    public bool useX;
    public bool useY;
    public Vector3 maxPosition;
    public Vector3 minPosition;

	// Update is called once per frame
	void Awake ()
    {
        float t = 0;
        if (topUICanvas.rect.height > 0)
        {
            float aspectRatio = topUICanvas.rect.width / topUICanvas.rect.height;
            t = Mathf.InverseLerp(minScaleAtRatio, maxScaleAtRatio, aspectRatio);
        }

        //scaling
        float scale = Mathf.Lerp(minScale, maxScale, t);
        transform.localScale = new Vector3(scale, scale, scale);
        
        //position
        if (useX)
        {
            transform.localPosition = Vector3.Lerp(new Vector3(minPosition.x, transform.localPosition.y, transform.localPosition.z), new Vector3(maxPosition.x, transform.localPosition.y, transform.localPosition.z), t);
        }
        if (useY)
        {
            transform.localPosition = Vector3.Lerp(new Vector3(transform.localPosition.x, minPosition.y, transform.localPosition.z), new Vector3(transform.localPosition.x, maxPosition.y, transform.localPosition.z), t);
        }
	}

    void OnEnable()
    {
        float t = 0;
        if (topUICanvas.rect.height > 0)
        {
            float aspectRatio = topUICanvas.rect.width / topUICanvas.rect.height;
            t = Mathf.InverseLerp(minScaleAtRatio, maxScaleAtRatio, aspectRatio);
        }
        float scale = Mathf.Lerp(minScale, maxScale, t);

        transform.localScale = new Vector3(scale, scale, scale);
        
    }
}
