﻿using UnityEngine;
using System.Collections;

public class TermsOfService : MonoBehaviour 
{
	public SharedUIElementsController sharedUIElementsController;
    public ScreenManager manager;

	private ElementAnimator elementAnimator;

    public Animator mainMenu;

    public bool showTermsOfService;

    public AudioSource click;

    public GameObject[] toDisableOnTermsAccept;

	void Awake () 
	{
		elementAnimator = GetComponent<ElementAnimator>();
        //if (!PlayerPrefs.HasKey("CompletedTermsOfUse") || (Application.isEditor && showTermsOfService))	
		{
			elementAnimator.On();
			sharedUIElementsController.ToOff();
		}
		
	}
	
	void Update () 
	{
	}

	public void AnimateOff()
	{
		PlayerPrefs.SetInt("CompletedTermsOfUse", 1);
		StartCoroutine(elementAnimator.AnimateOff());
		sharedUIElementsController.ToMenu();
        mainMenu.SetBool("isOn", true);
        mainMenu.SetBool("toNext", true);
	}

	public void Click()
	{
		manager.OpenPanelNoSlide(mainMenu);
		sharedUIElementsController.ToMenu();
		AnimateOff();
		click.Play();
		for (int i = 0; i < toDisableOnTermsAccept.Length; i++)
		{
			toDisableOnTermsAccept[i].SetActive(false);
		}
	}
}
