﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using MMT;

public class MoviePlayOnStart : MonoBehaviour {

    MobileMovieTexture texture;

	void Awake()
    {
        texture = GetComponent<MobileMovieTexture>();
	}

	void OnEnable ()
    {
        texture = GetComponent<MobileMovieTexture>();
        GetComponent<Image>().material.SetFloat("_TintColor", 0);
        StartCoroutine(FadeOn(0.4f));
        texture.PlaySpeed = 1;
        texture.PlayPosition = 0;
        texture.Play();
        //texture.PlaySpeed = 0;
	}

    void OnDisable()
    {
        texture = GetComponent<MobileMovieTexture>();

    }

    IEnumerator FadeOn(float duration)
    {
        float startTime = Time.time;
        while (Time.time < startTime + duration + 0.2f)
        {
            GetComponent<Image>().material.SetFloat("_TintColor", Mathf.InverseLerp(startTime + 0.1f, startTime + duration, Time.time));
            yield return new WaitForEndOfFrame();
        }
    }
}
