﻿using UnityEngine;
using System.Collections;

public class DisableOnGameStart : MonoBehaviour 
{
    public GameObject[] objects;
	
	void Awake () 
    {
	    foreach(GameObject obj in objects)
        {
            obj.SetActive(false);
        }
	}
}
