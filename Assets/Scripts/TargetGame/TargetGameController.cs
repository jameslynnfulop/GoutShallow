﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TargetGameController : MonoBehaviour
{
    public TargetDraggable[] options;
    public TargetDraggable correct;

    private Animator animator;

    public Animator controllerAnimator;
    public float timeBetweenGoOffscreen;

    public PageAnimator pageAnimator;

    public AudioSource incorrectSound;
    public AudioSource correctSound;

    void Awake()
    {
        for (int k = 0; k < options.Length; k++)
        {
            options[k].GetComponent<ElementAnimator>().Initialize();
        }
    }

	void Start ()
    {

	}

    void Reset()
    {
        animator.SetBool("Correct", false);
        animator.SetBool("Incorrect", false);
        controllerAnimator.SetBool("isResults", false);
        optionsRemoved = false;
        toResults = false;
        wrongAnswers = 0;
    }

    void OnDisable()
    {
        Reset();
    }

    void OnEnable()
    {
        animator = GetComponent<Animator>();
        pageAnimator.AllOff();
        for (int k = 0; k < options.Length; k++)
        {
            options[k].GetComponent<ElementAnimator>().Off();
            options[k].isAtDestination = false;

        }
        StartCoroutine(TakeOnScreen(0.25f));
    }

    bool optionsRemoved = false;
    bool toResults = false;
    int wrongAnswers = 0;
	void Update ()
    {
        for (int allOptionsIndex = 0; allOptionsIndex < options.Length && !toResults; allOptionsIndex++)
        {
            if (options[allOptionsIndex].isAtDestination)//picked one
            {
                if (options[allOptionsIndex] == correct)
                {
                    ToResults();
                    correctSound.Play();
                }
                else if (!optionsRemoved)
                {
                    StartCoroutine(options[allOptionsIndex].GetComponent<ElementAnimator>().AnimateOff());
                    options[allOptionsIndex].isAtDestination = false;
                    wrongAnswers++;
                    Debug.Log("hit");
                    incorrectSound.Play();
                    if (wrongAnswers == 2)
                    {
                        StartCoroutine(TakeOffScreen());
                        optionsRemoved = true;
                    }
                }
            }
        }
	}

    public void ToResults()
    {
        toResults = true;
        options[0].StopAllCoroutines();
        StartCoroutine(options[0].LerpToEnd());
        if (!optionsRemoved)
            StartCoroutine(TakeOffScreen());
        //choiceMade = true;
        controllerAnimator.SetBool("toResults", true);
    }

    IEnumerator TakeOnScreen(float startDelay)
    {
        yield return new WaitForSeconds(startDelay);
        for (int optionsIndex = 0; optionsIndex < options.Length; optionsIndex++)
        {
            options[optionsIndex].StopAllCoroutines();
            StartCoroutine(options[optionsIndex].GetComponent<ElementAnimator>().AnimateOn());
            yield return new WaitForSeconds(timeBetweenGoOffscreen);
        }
    }

    IEnumerator TakeOffScreen()
    {
        for (int optionsIndex = 1; optionsIndex < options.Length; optionsIndex++)
        {
            options[optionsIndex].isAtDestination = true;
            options[optionsIndex].StopAllCoroutines();
            //if (!options[optionsIndex].GetComponent<ElementAnimator>().isAnimating)
                StartCoroutine(options[optionsIndex].GetComponent<ElementAnimator>().AnimateOff());
            yield return new WaitForSeconds(timeBetweenGoOffscreen);
        }
    }
}
