﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class TargetDraggable : MonoBehaviour,
IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    //[HideInInspector]
    public bool isAtDestination;
    [HideInInspector]
    public bool wasFlung = false;

    public Transform destination;
    private Vector3 currentVelocity;
    public float smoothTime = 3;
    public float maxSpeed = 3;

    private Vector3 startingPosition;

    private float lastClickTime = 0;

    public Sprite atCenter;
	
    
    public bool isCorrectAnswer;
    public Transform target;
    public Transform onDisableParent;

    private Image image;
    private Sprite originalSprite;

	void Awake ()
    {
        startingPosition = transform.localPosition;
        image = GetComponent<Image>();
        originalSprite = image.sprite;
	}
	
    bool parentSet = false;
	void Update ()
    {
	    if (!parentSet)
        {
           transform.SetParent(onDisableParent, false);
           parentSet = true;
        }
	}

    void OnEnable()
    {
        //GetComponent<ElementAnimator>().Off();
        image.sprite = originalSprite;
        parentSet = false;
        GetComponent<AspectRatioGameObjectScaler>().enabled = true;
        isAtDestination = false;
    }

    void OnDisable()
    {
        Reset();
    }

    public void Reset()
    {
        isAtDestination = false;
        transform.localPosition = startingPosition;
        wasFlung = false;
        StopAllCoroutines();
    }
    
    public void OnDrag(PointerEventData data)
    {
        if (isAtDestination)
            return;

        Vector3 inputPos = Vector3.zero;
        inputPos.x = data.position.x;
        inputPos.y = data.position.y;
        inputPos = transform.parent.InverseTransformPoint(Camera.main.ScreenToWorldPoint(inputPos));
        inputPos.z = 0;
        transform.localPosition = inputPos;
    }

    public void OnEndDrag(PointerEventData data)
    {

    }

    public void OnPointerDown(PointerEventData data)
    {
        lastClickTime = Time.time;
    }

    public void OnPointerUp(PointerEventData data)
    {
       if (isAtDestination)
            return;

       if (destination != null && (Vector3.Distance(transform.localPosition, transform.parent.InverseTransformPoint(destination.position)) < 300f)
                                    || Time.time < lastClickTime + 0.5f)
        {
            StopAllCoroutines();
            GetComponent<ElementAnimator>().StopAllCoroutines();
            StartCoroutine(LerpToEnd());
        }
        else
        {
            StopAllCoroutines();
            GetComponent<ElementAnimator>().StopAllCoroutines();
            StartCoroutine(LerpToStart());
        }
    }

    public IEnumerator LerpToEnd()
    {
        while (Vector3.Distance(transform.localPosition, transform.parent.InverseTransformPoint(destination.position)) > 0.1f)
        {
            transform.localPosition = Vector3.SmoothDamp(transform.localPosition, transform.parent.InverseTransformPoint(destination.position), ref currentVelocity, smoothTime, maxSpeed);
            yield return new WaitForEndOfFrame();
        }
        isAtDestination = true;

        if (isCorrectAnswer)
        {
            image.sprite = atCenter;
            transform.SetParent(target, true);
            GetComponent<AspectRatioGameObjectScaler>().enabled = false;
        }
    }

    IEnumerator LerpToStart()
    {
        while (Vector3.Distance(transform.localPosition, startingPosition) > 1f)
        {
            transform.localPosition = Vector3.SmoothDamp(transform.localPosition, startingPosition, ref currentVelocity, smoothTime, maxSpeed);
            yield return new WaitForEndOfFrame();
        }
        isAtDestination = false;
    }

    /*IEnuemrator Fling()
    {
        wasFlung = true;
        while (Vector3.Distance(transform.localPosition, new Vector3(0,-3000,0)) > 0.1f)
        {
            transform.localPosition = Vector3.SmoothDamp(transform.localPosition, destination.localPosition, ref currentVelocity, smoothTime, maxSpeed);
            yield return new WaitForEndOfFrame();
        }
    }*/
}
