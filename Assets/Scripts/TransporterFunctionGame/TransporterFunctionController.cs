﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using MMT;

public class TransporterFunctionController : MonoBehaviour
{
	public SharedUIElementsController sharedUIElementsController;
	public MobileMovieTexture transitionInMovie;
    public Image mediumParticles;
    public Image mediumCellWalls;
    public Material closeCellWall;

	private Animator animator;

    public float delayToZoomIn;

    private CanvasGroup transporterCanvas;
    public CanvasGroup mediumMovies;
    public CanvasGroup closeMovies;

    public ElementAnimator submitButton;

	public Material waterLayer;
	public float waterLayerDistortionLevel = 25;

	public float delayToFadeInOnTransition = 1.55f;
	public float fadeInOnTransitionDuration = 0.275f;

	public GameObject leftMediumTransporter;
	public GameObject rightMediumTransporter;

	public GameObject leftCloseTransporter;
	public GameObject rightCloseTransporter;

	public GameObject closeView;

	public GameObject toZoom;

    void Awake()
    {
        submitButton.Initialize();
    }

	void Start ()
	{
        transporterCanvas = GetComponent<CanvasGroup>();
		animator = GetComponent<Animator>();
			//transitionInMovie.onFinished += TransitionMovieComplete;
	}



	void OnEnable()
	{
        sharedUIElementsController.ToOff();
        transporterCanvas = GetComponent<CanvasGroup>();
        mediumMovies.alpha = 0;
        closeMovies.alpha = 0;
        transporterCanvas.alpha = 0;
        //zoomMovie.gameObject.SetActive(false);
		if (!transitionInMovie.gameObject.activeSelf)
        {
            StartCoroutine(StartLoopingVideo(0, 0.01f));
        }
        else
        {
            mediumMovies.alpha = 0;
            closeMovies.alpha = 0;
            transporterCanvas.alpha = 0;
            mediumParticles.color = new Color(1, 1, 1, 0);
            mediumCellWalls.materialForRendering.SetFloat("_TintColor", 0);
            mediumParticles.gameObject.SetActive(false);
            mediumCellWalls.gameObject.SetActive(false);
            closeCellWall.SetFloat("_TintColor", 0);
			waterLayer.SetFloat("_BumpAmt", 0);
			StartCoroutine(StartLoopingVideo(delayToFadeInOnTransition, fadeInOnTransitionDuration));

        }
        submitButton.Off();
        //Invoke("ToGame", delayToZoomIn);

		//medium
		leftMediumTransporter.GetComponent<PerlinNoiseMovement>().enabled = true;
		leftMediumTransporter.GetComponent<MoveTowardsLocalPosition>().enabled = false;

		rightMediumTransporter.GetComponent<PerlinNoiseMovement>().enabled = true;
		rightMediumTransporter.GetComponent<MoveTowardsLocalPosition>().enabled = false;

		//close
		leftCloseTransporter.GetComponent<PerlinNoiseMovement>().enabled = false;
		rightCloseTransporter.GetComponent<PerlinNoiseMovement>().enabled = false;

		closeView.SetActive(false);
	}

    IEnumerator StartLoopingVideo(float delay, float duration)
    {
		toZoom.SetActive(false);
		yield return new WaitForSeconds(delay);
		toZoom.SetActive(true);
		animator.SetTrigger("TransitionComplete");
        mediumParticles.gameObject.SetActive(true);
        mediumCellWalls.gameObject.SetActive(true);
        closeMovies.alpha = 0;
        mediumParticles.color = new Color(1, 1, 1, 0);
        mediumCellWalls.materialForRendering.SetFloat("_TintColor", 0);
        //mediumMovies.alpha = 1;
        transporterCanvas.alpha = 0;
        mediumMovies.alpha = 0;
        float startTime = Time.time;
        while (Time.time < startTime + duration + 0.1f)
        {
            float t = Mathf.InverseLerp(startTime, startTime + duration, Time.time);
            transporterCanvas.alpha = t;
            mediumMovies.alpha = t;
            //transitionInMovie.GetComponent<Image>().material.SetFloat("_TintColor", Mathf.InverseLerp(startTime+ duration, startTime, Time.time));

            mediumParticles.color = new Color(1, 1, 1, t);
            mediumCellWalls.materialForRendering.SetFloat("_TintColor", t);
			waterLayer.SetFloat("_BumpAmt", Mathf.Lerp(0, waterLayerDistortionLevel, t));
            yield return new WaitForEndOfFrame();
        }
        transporterCanvas.alpha = 1;
        mediumMovies.alpha = 1;
        mediumParticles.color = new Color(1, 1, 1, 1);
		waterLayer.SetFloat("_BumpAmt", waterLayerDistortionLevel);
        mediumCellWalls.materialForRendering.SetFloat("_TintColor", 1);
        transitionInMovie.PlayPosition = 0;
        transitionInMovie.GetComponent<Image>().material.SetFloat("_TintColor", 0);
        transitionInMovie.gameObject.SetActive(false);
    }

	void TransitionMovieComplete(MobileMovieTexture sender)
	{


	}

	public void ToGame()
	{
		animator.SetBool("toGame", true);

        closeCellWall.SetFloat("_TintColor", 1);
		leftMediumTransporter.GetComponent<PerlinNoiseMovement>().enabled = false;
		leftMediumTransporter.GetComponent<MoveTowardsLocalPosition>().enabled = true;

		rightMediumTransporter.GetComponent<PerlinNoiseMovement>().enabled = false;
		rightMediumTransporter.GetComponent<MoveTowardsLocalPosition>().enabled = true;

		closeView.SetActive(true);
	}

	void OnDisable()
	{
		animator.SetBool("toGame", false);
        CancelInvoke();
	}

	public void TurnOnFullMenu()
	{
		sharedUIElementsController.ToAll();
	}

    public void CrossfadeMediumAndClose()
    {
        StartCoroutine(CrossfadeMediumClose());
    }

    private IEnumerator CrossfadeMediumClose()
    {
        float startTime = Time.time;
        while(Time.time < startTime + 0.5f + 1f)
        {
            float t = Mathf.InverseLerp(startTime + 0.5f, startTime, Time.time);
            mediumParticles.color = new Color(1, 1, 1, t);
            mediumCellWalls.materialForRendering.SetFloat("_TintColor", t);
			mediumMovies.alpha = t;

			closeMovies.alpha = Mathf.Lerp(1,0,t);

            yield return new WaitForEndOfFrame();
        }
        mediumParticles.gameObject.SetActive(false);
        mediumCellWalls.gameObject.SetActive(false);

		leftCloseTransporter.GetComponent<PerlinNoiseMovement>().enabled = true;
		rightCloseTransporter.GetComponent<PerlinNoiseMovement>().enabled = true;
    }

    public void SwitchClicked()
    {
        if (!submitButton.isOn)
            StartCoroutine(submitButton.AnimateOn());
    }
}
