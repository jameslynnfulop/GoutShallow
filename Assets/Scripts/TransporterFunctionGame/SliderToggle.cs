﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Slider))]
public class SliderToggle : MonoBehaviour
{
	public Text on;

	public bool isOn = true;

	private Slider slider;

    float startingSliderValue = 0;
	// Use this for initialization
	void Start () 
	{
		slider = GetComponent<Slider>();
        startingSliderValue = slider.value;
	}
	
	public void ToggleText()
	{
		on.gameObject.SetActive(!on.gameObject.activeSelf);
		if (slider.value == 1)
			slider.value = 0;
		else
			slider.value = 1;
	}

    
    void OnDisable()
    {
        slider.value = startingSliderValue;
    }
    
	// Update is called once per frame
	void Update () 
	{
    	if (slider.value == 1)
			isOn = true;
		else
			isOn = false;
	}


}
