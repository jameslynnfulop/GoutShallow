using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Transporter : MonoBehaviour 
{
	public Toggle toggle;

    public Image heavyFlow;
    public Image lightFlow;
    public float fadeDuration = 0.5f;

    private bool startingValue;
    void Awake()
    {
        startingValue = toggle.isOn;
    }
	
	void Start () 
	{
		
		
		
	}

	void OnEnable()
	{
		Start ();
        toggle.isOn = startingValue;
	}

    private bool toggleValue;
	void Update () 
	{
	    if (toggle.isOn != toggleValue)//execute on value change, not every frame
        {
            StopAllCoroutines();
            toggleValue = toggle.isOn;
            if (toggle.isOn)
            {
                StartCoroutine(FadeAlphaOn(heavyFlow, fadeDuration));
                StartCoroutine(FadeAlphaOff(lightFlow, fadeDuration));
            }
            else{

                StartCoroutine(FadeAlphaOff(heavyFlow, fadeDuration));
                StartCoroutine(FadeAlphaOn(lightFlow, fadeDuration));
            }
            
        }
        
		
	}

    IEnumerator FadeAlphaOn(Image image, float duration)
    {
        float startTime = Time.time;
        while (Time.time < startTime + duration + 0.5f)
        {
            float t = Mathf.InverseLerp(0, duration, Time.time - startTime);
            image.color = new Color(1,1,1, t);
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator FadeAlphaOff(Image image, float duration)
    {
        float startTime = Time.time;
        while (Time.time < startTime + duration + 0.5f)
        {
            float t = Mathf.InverseLerp(duration, 0, Time.time - startTime);
            image.color = new Color(1,1,1, t);
            yield return new WaitForEndOfFrame();
        }
    }
}
