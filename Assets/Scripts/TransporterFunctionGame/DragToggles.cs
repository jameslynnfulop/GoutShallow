﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class DragToggles : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler, IEndDragHandler
{

	Toggle toggle;

	public float dragDeltaThreshold = 3;
	// Use this for initialization
	void Start ()
	{
		toggle = GetComponent<Toggle>();
	}

    float lastTimeDown;

    public TransporterFunctionController controller;

    public void OnPointerDown(PointerEventData data)
    {
        lastTimeDown = Time.time;
    }

    public float maxClickDownUpDelta = 0.15f;
    public void OnPointerUp(PointerEventData data)
    {
        if (Time.time < lastTimeDown + maxClickDownUpDelta)
        {
            toggle.isOn = !toggle.isOn;
            controller.SwitchClicked();
        }
    }

	public void OnDrag(PointerEventData data)
	{
/*
		Vector3 inputPos = Vector3.zero;
        inputPos.x = data.position.x;
        inputPos.y = data.position.y;
		inputPos = transform.parent.InverseTransformPoint(Camera.main.ScreenToWorldPoint(inputPos));
		if (!rectTransform.rect.Contains(inputPos))
			return;
*/
		if ((!toggle.isOn && data.delta.x > dragDeltaThreshold)//is off and finger is moving to the right
			|| (toggle.isOn && data.delta.x < -dragDeltaThreshold))//is on and finger is moving to left
		{
			toggle.isOn = !toggle.isOn;
			controller.SwitchClicked();
		}
	}

	public void OnEndDrag(PointerEventData data)
	{
		//toggle.isOn = !toggle.isOn;


		//Debug.Log("drag toggled");
	}
}
