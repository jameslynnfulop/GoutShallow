﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class GlomerulusFilterDragDrop : MonoBehaviour,
IDragHandler, IPointerDownHandler, IPointerUpHandler
{
	public Transform destination;

    private Vector3 currentVelocity;

    public float smoothTime = 3;
    public float maxSpeed = 3;

    //public Image background;
    //private Sprite startingSprite;
    //public Sprite resultSprite;

    private bool isHome = false;

    private Vector3 startingPosition;

    public GlomerulusController controller;

    private PurineExogenousSource splineAnimator;

    private Vector3 startingScale;

    public ElementAnimator instructions;

    public AudioSource clickSound;

    public GameObject highlight;

    void Start()
    {
        splineAnimator = GetComponent<PurineExogenousSource>();
        startingPosition = transform.localPosition;
        startingScale = transform.localScale;
        instructions.Initialize();
    }

    public void OnDrag(PointerEventData data)
    {
        if (isHome)
            return;

        Vector3 inputPos = Vector3.zero;
        inputPos.x = data.position.x;
        inputPos.y = data.position.y;
        inputPos = transform.parent.InverseTransformPoint(Camera.main.ScreenToWorldPoint(inputPos));
        inputPos.z = 0;

        transform.localPosition = inputPos;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
    }

    private float pointerDownTime = 0;
    public void OnPointerDown(PointerEventData eventData)
    {
        pointerDownTime = Time.time;
        StartCoroutine(instructions.AnimateOff());
        clickSound.Play();
        highlight.SetActive(true);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (isHome)
            return;

        if (destination != null && ((Vector3.Distance(transform.localPosition, destination.localPosition) < 400f)
                                     || Time.time < pointerDownTime + 0.1f))
        {
            GoToEnd();
        }
        else
        {
            StartCoroutine(LerpToStart());
        }
    }

    public void GoToEnd()
    {
        highlight.SetActive(false);
        StartCoroutine(LerpToEnd());
        if (!instructions.isAnimating)//only false when we hit this code from fast forward button
            StartCoroutine(instructions.AnimateOff());
    }

    IEnumerator LerpToEnd()
    {
        while (Vector3.Distance(transform.localPosition, destination.localPosition) > 2f)
        {
            transform.localPosition = Vector3.SmoothDamp(transform.localPosition, destination.localPosition, ref currentVelocity, smoothTime, maxSpeed);
            yield return new WaitForEndOfFrame();
        }
        isHome = true;
 //       background.sprite = resultSprite;
        controller.ToResult();
        StartCoroutine(FollowSpline());

    }

    IEnumerator LerpToStart()
    {
        while (Vector3.Distance(transform.localPosition, startingPosition) > 2f)
        {
            transform.localPosition = Vector3.SmoothDamp(transform.localPosition, startingPosition, ref currentVelocity, smoothTime, maxSpeed);
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator FollowSpline()
    {
        float startTime = Time.time;
        splineAnimator.enabled = true;
        while (Time.time < startTime + 0.5f + 1f)
        {
            splineAnimator.t = Mathf.InverseLerp(startTime, startTime + 0.5f, Time.time);
            yield return new WaitForEndOfFrame();
        }
        splineAnimator.enabled = false;
    }

    void OnDisable()
    {
        Reset();
    }

    void Reset()
    {
        StopAllCoroutines();
        transform.localPosition = startingPosition;
    //    background.sprite = startingSprite;
        isHome = false;
        transform.localScale = startingScale;
        instructions.On();
		splineAnimator.enabled = false;
    }
}
