﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using MMT;

[RequireComponent(typeof(Animator))]
public class GlomerulusController : MonoBehaviour
{
    //public ResetAllMMTOnObject resetAllComponent;

    public GameObject glomStartStillImage;
	public MobileMovieTexture toGameMovie;
	//public MobileMovieTexture toResultMovie;
    public Flipbook toResultFlipbook;
	//public MobileMovieTexture resultLoopMovie;
    public GameObject transparentLoop;
    public Flipbook transparentFlipbook;

    public CanvasGroup glomBackgroundsMaster;

	private Animator animator;

	public SharedUIElementsController sharedUIElementsController;

    public ElementAnimator fastForwardButton;
    public ElementAnimator toTransporterFunctionButton;

    public Image[] toFadeInOnGame;

    public Image[] fadeOnAtResults;
    public Image[] fadeOffAtResults;

    //public GameObject substateA;
    //public GameObject substateB;

    [Header("Reactive")]
    public RectTransform topUICanvas;
    public float maxScaleAtRatio = 1.33f;
    public float minScaleAtRatio = 1.77f;

    public Vector3 maxPosition;
    public Vector3 minPosition;

	// Use this for initialization
	void Start ()
	{
		animator = GetComponent<Animator>();
		toGameMovie.onFinished += OnGameVideoFinish;
		//toResultMovie.onFinished += OnResultVideoFinish;
        toTransporterFunctionButton.Initialize();
        fastForwardButton.Initialize();
        toTransporterFunctionButton.Off();
	}

    void OnEnable()
    {
        //resetAllComponent.ResetAll();
        Invoke("Derp", 0.1f);
        //substateB.SetActive(false);
        sharedUIElementsController.ToMenuAndReferences();

        for (int i = 0; i < toFadeInOnGame.Length; i++)
        {
            toFadeInOnGame[i].gameObject.SetActive(false);
            toFadeInOnGame[i].color = new Color(1,1,1,0);
        }
        float aspectRatio = topUICanvas.rect.width / topUICanvas.rect.height;
        float t = Mathf.InverseLerp(minScaleAtRatio, maxScaleAtRatio, aspectRatio);
        toGameMovie.transform.localPosition = Vector3.Lerp(new Vector3(transform.localPosition.x, minPosition.y, transform.localPosition.z), new Vector3(transform.localPosition.x, maxPosition.y, transform.localPosition.z), t);
        glomStartStillImage.transform.localPosition = Vector3.Lerp(new Vector3(transform.localPosition.x, minPosition.y, transform.localPosition.z), new Vector3(transform.localPosition.x, maxPosition.y, transform.localPosition.z), t);
        if (Mathf.Abs(aspectRatio - minScaleAtRatio) < 0.1f)
        {
            toGameMovie.transform.localScale = new Vector3(0.85f,0.85f,0.85f);
            glomStartStillImage.transform.localScale = new Vector3(0.85f,0.85f,0.85f);
        }
        glomBackgroundsMaster.alpha = 1;
    }

    private void Derp()
    {

        toGameMovie.gameObject.SetActive(true);
        //toGameMovie.Play();
        //toGameMovie.PlayPosition = 0;
        //toGameMovie.PlaySpeed = 0;
    }

    public void ToGame()
    {
        toGameMovie.PlaySpeed = 1;
        toGameMovie.Play();
        animator.SetTrigger("ToGameTransition");
        glomStartStillImage.SetActive(false);
        StartCoroutine(MoveVideoDown(1));
    }

	void OnGameVideoFinish(MobileMovieTexture sender)
	{
		animator.SetTrigger("ToGame");
		sharedUIElementsController.ToAll();

        //toGameMovie.gameObject.SetActive(false);
        toResultFlipbook.gameObject.SetActive(true);
        toResultFlipbook.GetComponent<Image>().sprite = toResultFlipbook.sprites[0];
        StartCoroutine(fastForwardButton.AnimateOn());
        for (int i = 0; i < toFadeInOnGame.Length; i++)
        {
            toFadeInOnGame[i].gameObject.SetActive(true);
            toFadeInOnGame[i].color = new Color(1,1,1,0);
        }
        StartCoroutine(CrossfadeVideoAndImage(toGameMovie.GetComponent<RawImage>(),
                                              0.5f));
        //substateB.SetActive(true);
	}

    public void ToResult()
    {
        //toResultMovie.PlaySpeed = 1;
        toResultFlipbook.playForward = true;
        GetComponent<DockBounceAccessor>().TurnOnDockBounce();
    }

    bool completedToTranparent;
    void Update()
    {
        if (toResultFlipbook.imageIndex >= toResultFlipbook.sprites.Length -1 && !completedToTranparent)
        {
            OnResultVideoFinish();
            completedToTranparent = true;
        }
    }

	//void OnResultVideoFinish(MobileMovieTexture sender)
    void OnResultVideoFinish()
    {
        animator.SetTrigger("ToResult");
        //toResultMovie.gameObject.SetActive(false);
        //toResultFlipbook.gameObject.SetActive(false);
        //resultLoopMovie.gameObject.SetActive(true);
        transparentLoop.gameObject.SetActive(true);
       // resultLoopMovie.Play();
        //resultLoopMovie.PlayPosition = 0;
        //resultLoopMovie.PlaySpeed = 1;
        transparentFlipbook.imageIndex = 0;
        transparentFlipbook.GetComponent<Image>().sprite = transparentFlipbook.sprites[0];
        transparentFlipbook.playForward = true;
        StartCoroutine(fastForwardButton.AnimateOff());
        StartCoroutine(toTransporterFunctionButton.AnimateOn());
        StartCoroutine(CrossfadeImages(fadeOffAtResults, fadeOnAtResults, 0.15f));
	}

	void OnDisable()
	{
		toGameMovie.Play();
		toGameMovie.PlayPosition = 0;
		toGameMovie.PlaySpeed = 0;

        transparentLoop.SetActive(false);

        toResultFlipbook.playForward = false;
        toResultFlipbook.imageIndex = 0;
        completedToTranparent = false;

        toResultFlipbook.gameObject.SetActive(false);

        toTransporterFunctionButton.Off();
        glomStartStillImage.SetActive(true);
        toGameMovie.GetComponent<RawImage>().material.SetFloat("_TintColor", 1);

        for (int i = 0; i < toFadeInOnGame.Length; i++)
        {
            toFadeInOnGame[i].color = new Color(1,1,1,1);
        }

        for (int i = 0; i < fadeOnAtResults.Length; i++)
        {
            fadeOnAtResults[i].color = new Color(1,1,1,1);
        }

        for (int i = 0; i < fadeOffAtResults.Length; i++)
        {
            fadeOffAtResults[i].color = new Color(1,1,1,1);
        }
	}

    IEnumerator CrossfadeVideoAndImage(RawImage videoImage, float duration)
    {
        float startTime = Time.time;
        while (Time.time < startTime + duration + 0.1f)
        {
            videoImage.materialForRendering.SetFloat("_TintColor", Mathf.InverseLerp(startTime + duration, startTime, Time.time));
            for (int i = 0; i < toFadeInOnGame.Length; i++)
            {
                toFadeInOnGame[i].color = new Color(1,1,1,1);//Mathf.InverseLerp(startTime, startTime + duration, Time.time)
            }
            yield return new WaitForEndOfFrame();
        }
        videoImage.gameObject.SetActive(false);
    }

    IEnumerator CrossfadeImages(Image[] a, Image[] b, float duration)
    {
        /*float startTime = Time.time;
        while (Time.time < startTime + duration + 0.1f)
        {
            for (int i = 0; i < a.Length; i++)
            {
                a[i].color = new Color(1,1,1,Mathf.InverseLerp(startTime + duration, startTime, Time.time));
            }
            for (int i = 0; i < b.Length; i++)
            {
                b[i].color = new Color(1,1,1,Mathf.InverseLerp(startTime,startTime + duration, Time.time));
            }


        }*/
        for (int i = 0; i < a.Length; i++)
        {
            a[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < b.Length; i++)
        {
            b[i].gameObject.SetActive(true);
        }
        yield return new WaitForEndOfFrame();
    }

     IEnumerator MoveVideoDown(float duration)
     {
         float aspectRatio = topUICanvas.rect.width / topUICanvas.rect.height;
         float maxT = Mathf.InverseLerp(minScaleAtRatio, maxScaleAtRatio, aspectRatio);
         float startTime = Time.time;
         while (Time.time < startTime + duration)
         {
             float t = Mathf.Lerp(maxT, 1, Mathf.InverseLerp(startTime, startTime + duration, Time.time));
             toGameMovie.transform.localPosition = Vector3.Lerp(new Vector3(transform.localPosition.x, minPosition.y, transform.localPosition.z),
                                                                new Vector3(transform.localPosition.x, maxPosition.y, transform.localPosition.z),
                                                                t);

            if (Mathf.Abs(aspectRatio - minScaleAtRatio) < 0.1f)
                toGameMovie.transform.localScale = Vector3.Lerp(new Vector3(0.85f,0.85f,0.85f), new Vector3(1,1,1), Mathf.InverseLerp(startTime, startTime + duration, Time.time));
            yield return new WaitForEndOfFrame();
         }
     }

     public void FadeToTransitionVideo(float duration)
     {
         StartCoroutine(ToTransitionVideoFader(duration));
     }

     IEnumerator ToTransitionVideoFader(float duration)
     {
         float startTime = Time.time;
         yield return new WaitForSeconds(0.5f);
         while (Time.time < startTime + duration)
         {
             glomBackgroundsMaster.alpha = Mathf.Lerp(1, 0, Mathf.InverseLerp(startTime, startTime + duration, Time.time));

             yield return new WaitForEndOfFrame();
         }
         glomBackgroundsMaster.gameObject.SetActive(false);
     }
}
