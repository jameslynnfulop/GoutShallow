﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BackButtonModule : MonoBehaviour {
	public ScreenManager screenManager;
	public Image fakeBackground;
    public Button otherButton;
    public BackButtonCallback backButton;

	void OnEnable()
	{
			screenManager.fakeBackground = fakeBackground;
            backButton.otherButtonEvent = otherButton.onClick;
            backButton.BackOneModuleOnBackButtonClick();
	}

}
