﻿using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

public class PurineAtom : MonoBehaviour,
IDragHandler, IPointerUpHandler, IPointerDownHandler
{

    public Transform destination;
    //[HideInInspector]
    public Transform originalDestination;
    public Element element;

    private Vector3 currentVelocity;

    public float smoothTime = 3;
    public float maxSpeed = 3;

    public bool inTransit = false;
    public bool isHome = false;

    public Sprite frame;

    private Vector3 startingPosition;
    private PerlinNoiseMovement noiseMovement;

    public PurineGameController purineGameController;

    public AudioSource clickSound;

    public bool interactable = true;

	void Awake ()
    {
	    startingPosition = transform.localPosition;
        noiseMovement = GetComponent<PerlinNoiseMovement>();
        //originalDestination = destination;
	}

    void OnEnable()
    {
        interactable = true;
    }

    void OnDisable()
    {
        Reset();
    }

	// Update is called once per frame
	void Update ()
  {

	}

    void Reset()
    {
        mouseDownTime = 0;
        isHome = false;
        transform.localPosition = startingPosition;
        noiseMovement.enabled = true;
        inTransit = false;
    }

    public float mouseDownTime = 0;

    public void OnDrag(PointerEventData data)
    {
        if (!interactable)
            return;

        if (isHome)
        {
            noiseMovement.enabled = false;
            return;
        }

        //purineGameController.SendMessage("DraggedAtom");
        Vector3 inputPos = Vector3.zero;
        inputPos.x = data.position.x;
        inputPos.y = data.position.y;
        inputPos = transform.parent.InverseTransformPoint(Camera.main.ScreenToWorldPoint(inputPos));
        inputPos.z = 0;

        transform.localPosition = inputPos;
        noiseMovement.enabled = false;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!interactable)
            return;

        purineGameController.HighlightPosition(element);
        mouseDownTime = Time.time;
        clickSound.Play();
    }


    public void OnEndDrag(PointerEventData eventData)
    {
        //StartCoroutine(LerpToEnd());
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (!interactable)
            return;

      purineGameController.KillHighlight();
        if (isHome)
            return;

//        Debug.Log("pointer up");

        if (destination != null && (Vector3.Distance(transform.localPosition, destination.localPosition) < 200f
            || Time.time - mouseDownTime < 0.25f))
        {
            StartCoroutine(LerpToEnd());
        }
        else
        {
            StartCoroutine(LerpToStart());
        }
    }

    public IEnumerator LerpToEnd()
    {
        inTransit = true;
        noiseMovement.enabled = false;
        if (destination == null)
            destination = originalDestination;
        while (Vector3.Distance(transform.localPosition, destination.localPosition) > 1f)
        {
            transform.localPosition = Vector3.SmoothDamp(transform.localPosition, destination.localPosition, ref currentVelocity, smoothTime, maxSpeed);
            //isHome = false;
            yield return new WaitForEndOfFrame();
        }
        inTransit = false;

        isHome = true;
    }

    public IEnumerator LerpToStart()
    {
        noiseMovement.enabled = false;
        while (Vector3.Distance(transform.localPosition, startingPosition) > 10f)
        {
            transform.localPosition = Vector3.SmoothDamp(transform.localPosition, startingPosition, ref currentVelocity, smoothTime, maxSpeed);
            yield return new WaitForEndOfFrame();
        }
        noiseMovement.enabled = true;
    }
}
