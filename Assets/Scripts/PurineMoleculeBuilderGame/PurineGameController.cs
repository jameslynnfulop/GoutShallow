﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[System.Serializable]
public enum Element
{
	H,
	O,
	N
}

public class PurineGameController : MonoBehaviour
{
	[System.Serializable]
	public struct Destination
	{
		public Transform destination;
		public Element element;
	}

	[System.Serializable]
	public struct PurineStage
	{
		public GameObject stageMaster;
		public PurineAtom[] stageAtoms;
		public Destination[] destinations;
        public PageDelayPair[] turnOn;
        public PageDelayPair[] turnOff;
	}

    [System.Serializable]
    public struct PageDelayPair
    {
        public GameObject page;
        public float delay;
    }

	public PurineStage[] stages;
    public ElementAnimator[] instructions;

	public Highlight highlight;

	public GameObject background;

	int currentStage = 0;
	int currentDestination = 0;

	private bool isDone;

    public Animator purineGameController;
    public PageAnimator pageAnimator;

    public PageDelayPair[] gameCompleteTurnOn;
    public PageDelayPair[] gameCompleteTurnOff;

    public PageDelayPair[] resultsTurnOn;
    public PageDelayPair[] resultsTurnOff;

    public float fastForwardNextStageDelay = 1;
    public float fastForwardAtomZoomDelay = 1;
    public float gameCompleteToResultsDelay = 2;
    public Button fastForwardButton;

    public ElementAnimator[] resultsLabels;

    public float resultsFlyOnInitialDelay = 2;
    public float delayBetweenResultsFlyOn = 1;

    public AudioSource wooshSound;

    public CrossfadeCanvasGroup backgroundElements;

    void Start()
    {
        foreach (ElementAnimator element in resultsLabels)
        {
            element.Initialize();
            element.Off();
        }
    }

    void Reset()
    {

        stages[0].stageMaster.SetActive(true);
        currentStage = 0;
        currentDestination = 0;
        highlight.transform.position = new Vector3(1000,1000,0);
        isDone = false;
        isFastForward = false;
        noMoreInstructions = false;
        GetComponent<BackgroundRequester>().enabled = false;
        moduleStartTime = 0;
        CancelInvoke();
        StopAllCoroutines();
        foreach (ElementAnimator element in resultsLabels)
        {
            element.Off();
        }
		for (int i = 0; i < stages.Length; i++)
		{
			for (int k = 0; k < stages[i].stageAtoms.Length; k++)
			{
				stages[i].stageAtoms[k].interactable = true;
			}
		}

    }


    void OnDisable()
    {
        Reset();
    }

    void OnEnable()
    {
        backgroundElements.SetNoAlpha();
        pageAnimator.AllOff();
        foreach (PageDelayPair pageDelayPair in stages[0].turnOn)
        {
            pageAnimator.OnSubpage(pageDelayPair.page.name);
        }
        StartCoroutine(DisableTargetForSeconds(1));
        fastForwardButton.interactable = true;
        fadeOnBackground = false;
    }

    private bool isFastForward = false;
    private bool noMoreInstructions = false;
    private float moduleStartTime = 0;
    private bool fadeOnBackground = false;
	void Update ()
	{
        AnimatorStateInfo stateInfo = purineGameController.GetCurrentAnimatorStateInfo(0);
        if (stateInfo.IsName("On"))
        {
            if (!fadeOnBackground)
            {
                backgroundElements.CrossfadeOn(0.8f);
                fadeOnBackground = true;
            }
            //backgroundElements.SetFullAlpha();

            if (moduleStartTime == 0)
                moduleStartTime = Time.time;

            if (GetComponent<BackgroundRequester>().enabled ==false)
                GetComponent<BackgroundRequester>().enabled = true;
        }


		if (isDone)//finished
        {
            highlight.transform.position = new Vector3(10000, 10000, 10000);
			return;
        }

        if (isFastForward)
        {
            if (noMoreInstructions)
            {
                foreach (ElementAnimator instruction in instructions)
                {
                    instruction.Off();
                }
            }

            return;
        }



		for (int i = 0; i < stages[currentStage].stageAtoms.Length; i++)
		{
			if (!stages[currentStage].stageAtoms[i].isHome && !stages[currentStage].stageAtoms[i].inTransit &&
				stages[currentStage].stageAtoms[i].element == stages[currentStage].destinations[currentDestination].element)
			{
				stages[currentStage].stageAtoms[i].destination = stages[currentStage].destinations[currentDestination].destination;
			}
            else if (!stages[currentStage].stageAtoms[i].inTransit)
            {
                stages[currentStage].stageAtoms[i].destination = null;
            }
		}

		int atomsAtDestination = 0;
		for (int i = 0; i < stages[currentStage].stageAtoms.Length; i++)
		{
			if (stages[currentStage].stageAtoms[i].isHome || stages[currentStage].stageAtoms[i].inTransit)
			{
				atomsAtDestination++;

            }
		}
        if (currentDestination != atomsAtDestination)
        {
            currentDestination = atomsAtDestination;
            //HighlightPosition();

        }



		if (atomsAtDestination == stages[currentStage].stageAtoms.Length)
		{
			if (currentStage < stages.Length - 1)
			{
                ToNextStage();
			}
			else
			{
                ToGameComplete();
			}
		}
	}

    private void ToNextStage()
    {
        currentStage++;
        foreach (PageDelayPair pageDelayPair in stages[currentStage].turnOn)
        {
            pageAnimator.AnimateOnSubpage(pageDelayPair.page.name, pageDelayPair.delay);
        }
        foreach (PageDelayPair pageDelayPair in stages[currentStage].turnOff)
        {
            pageAnimator.AnimateOffSubpage(pageDelayPair.page.name, pageDelayPair.delay);
        }


        stages[currentStage].stageMaster.SetActive(true);
        currentDestination = 0;
        int stageNumberPlusOne = currentStage + 1;
        purineGameController.SetTrigger("to" + stageNumberPlusOne.ToString());
        StartCoroutine(DisableTargetForSeconds(1));
        wooshSound.Play();
    }

    IEnumerator DisableTargetForSeconds(float seconds)
    {
        //destinationHighlight.gameObject.SetActive(false);
        yield return new WaitForSeconds(seconds);
        //destinationHighlight.gameObject.SetActive(true);
        //HighlightPosition();
    }

	public void EnableBackground()
	{
		background.SetActive(true);
	}

    public void ToGameComplete()
    {
//        purineGameController.SetTrigger("toGameComplete");
        isDone = true;
        foreach (PageDelayPair pageDelayPair in gameCompleteTurnOn)
        {
            pageAnimator.AnimateOnSubpage(pageDelayPair.page.name, pageDelayPair.delay);
        }
        foreach (PageDelayPair pageDelayPair in gameCompleteTurnOff)
        {
            pageAnimator.AnimateOffSubpage(pageDelayPair.page.name, pageDelayPair.delay);
        }
        Invoke("ToResults", gameCompleteToResultsDelay);
        wooshSound.Play();
    }

    void ToNextStageFastForward()
    {
        noMoreInstructions = true;
        ToNextStage();
        Invoke("AnimateElementsToEndPosition", fastForwardAtomZoomDelay);
    }

    void AnimateElementsToEndPosition()
    {
        foreach (PurineAtom atom in stages[currentStage].stageAtoms)
        {
            atom.destination = atom.originalDestination;
            StartCoroutine(atom.LerpToEnd());
            if (atom.GetComponents<OnClickAnimateElement>() != null)//to get frame aditions
            {
                foreach (OnClickAnimateElement onclick in atom.GetComponents<OnClickAnimateElement>())//brings on extra frame elements
                {
                    onclick.AnimateOther();
                }
            }
        }

        if (currentStage < stages.Length -1)
        {
            Invoke("ToNextStageFastForward", fastForwardNextStageDelay);
        }
        else
        {
            Invoke("ToGameComplete", fastForwardNextStageDelay);
        }
    }
    public void FastForward()
    {
        isFastForward = true;
		for (int i = 0; i < stages.Length; i++)
		{
			for (int k = 0; k < stages[i].stageAtoms.Length; k++)
			{
				stages[i].stageAtoms[k].interactable = false;
			}
		}
        AnimateElementsToEndPosition();
    }

    public void ToResults()
    {
        GetComponent<DockBounceAccessor>().TurnOnDockBounce();
        purineGameController.SetTrigger("toResults");
        foreach (PageDelayPair pageDelayPair in resultsTurnOff)
        {
            pageAnimator.AnimateOffSubpage(pageDelayPair.page.name, pageDelayPair.delay);
        }

        foreach (PageDelayPair pageDelayPair in resultsTurnOn)
        {
            pageAnimator.AnimateOnSubpage(pageDelayPair.page.name, pageDelayPair.delay);
        }

        wooshSound.PlayDelayed(0.1f);
        Invoke("FlyOnResultsStuff", resultsFlyOnInitialDelay);
    }

    private void FlyOnResultsStuff()
    {
        float delay = 0;
        foreach (ElementAnimator element in resultsLabels)
        {
            StartCoroutine(element.AnimateOn(delay));
            delay += delayBetweenResultsFlyOn;
            //StartCoroutine(element.AnimateOff(delay));
        }

    }

    public void HighlightPosition(Element elementOfDraggedAtom)
    {
        //position and color the highlight object
        if (currentStage < stages.Length)
        {
            if (currentDestination < stages[currentStage].destinations.Length &&
                elementOfDraggedAtom == stages[currentStage].destinations[currentDestination].element)
            {
                highlight.transform.position = stages[currentStage].destinations[currentDestination].destination.position;
                switch (stages[currentStage].destinations[currentDestination].element)
                {
                    case Element.H:
                    highlight.Orange();
                    break;

                    case Element.O:
                    highlight.Blue();
                    break;

                    case Element.N:
                    highlight.Wine();
                    break;
                }
            }
            else
            {
                incorrectSound.Play();
            }
        }
    }

    public AudioSource incorrectSound;
	public void KillHighlight()
	{
		highlight.Stop();
	}
}
