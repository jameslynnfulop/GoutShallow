﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class OnPointerUpTermsOfServiceButton : MonoBehaviour , IPointerUpHandler
{
	public TermsOfService terms;

    public void OnPointerUp(PointerEventData eventData)
    {
    	terms.Click();    
    }
}

