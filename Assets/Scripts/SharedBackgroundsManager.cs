﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct BackgroundPagesPair
{
	public GameObject background;
	public GameObject[] pages;
}

public class SharedBackgroundsManager : MonoBehaviour
{

	//public BackgroundPagesPair[] pairs;

	public GameObject[] backgrounds;

	private GameObject currentBackground;
    //private Transform transformToMatch;

	void Start ()
	{

	}

	void Update ()
	{
    //    currentBackground.transform.localPosition = transformToMatch.localPosition;
	}

	/*public void CheckBackground(GameObject newPage)
	{
		foreach(BackgroundPagesPair checkPair in pairs)
		{
			foreach (GameObject checkPage in checkPair.pages)
			{
				if (newPage == checkPage)
				{
					if (checkPair.background != currentBackground)
					{
						//TODO: Do a better transition here, probably glue background to page as it slides into place
						currentBackground.SetActive(false);
						checkPair.background.SetActive(true);
						currentBackground = checkPair.background;
					}
				}
			}
		}
	}*/

	public void GetBackground(GameObject background, Transform parent)
	{
		foreach (GameObject go in backgrounds)
		{
			if (go == background && !go.activeSelf)
			{
				go.SetActive(true);
                //transformToMatch = parent;
				if (currentBackground != null)
                {
                    currentBackground.SetActive(false);

                }


				currentBackground = go;
			}
		}
	}

    /*public void GetBackgroundGlueToObj(GameObject background, Transform backgroundRequester)
    {
        GetBackground(background);
        background.transform.localPosition = backgroundRequester.localPosition;
    }*/
}
