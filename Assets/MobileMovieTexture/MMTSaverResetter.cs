﻿using UnityEngine;
using System.Collections;
using MMT;

public class MMTSaverResetter : MonoBehaviour 
{
    private struct Settings
    {
        public bool enabled;
        public int LoopCount;
        public float PlaySpeed;
    }

    public MobileMovieTexture mmt;

    private Settings settings;

	public void Init() 
    {
        settings.enabled = mmt.enabled;
        settings.LoopCount = mmt.LoopCount;
        settings.PlaySpeed = mmt.m_playSpeed;
	}
	
	public void Reset()
    {
        mmt.enabled = settings.enabled;
        /*mmt.LoopCount = settings.LoopCount;
        mmt.PlaySpeed = settings.PlaySpeed;
        mmt.Play();
        mmt.PlayPosition = 0;        */
    }
}
