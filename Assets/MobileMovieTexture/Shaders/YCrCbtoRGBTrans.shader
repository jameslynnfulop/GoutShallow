Shader "Color Space/YCbCrtoRGB Trans" 
{
    Properties 
    {
        _YTex ("Y (RGB)", 2D) = "black" {}
        _CrTex ("Cr (RGB)", 2D) = "gray" {}
        _CbTex ("Cb (RGB)", 2D) = "gray" {}
        _TintColor ("Tint Color", Range (0.0, 1.0)) = 1.0

        _StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255

		_ColorMask ("Color Mask", Float) = 15
    }
    SubShader 
    {
		Tags 
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
		}

		Stencil
		{
			Ref [_Stencil]
			Comp [_StencilComp]
			Pass [_StencilOp] 
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
		}
		
		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask [_ColorMask]
        
        Pass 
        {


			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			sampler2D _YTex;
			sampler2D _CbTex;
			sampler2D _CrTex;
			
			#include "YCbCrtoRGB.cginc"
			
			float _TintColor;
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f 
			{
				float4  pos : SV_POSITION;
				half2  uvY : TEXCOORD0;
				half2  uvCbCr : TEXCOORD1;

				fixed4 color : COLOR;
			};

			float4 _YTex_ST;
			float4 _CbTex_ST;

			v2f vert (appdata_t v)
			{
				v2f o;
				o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
				o.uvY = TRANSFORM_TEX (v.texcoord, _YTex);
				o.uvCbCr = TRANSFORM_TEX (v.texcoord, _CbTex);

				v.color.w = _TintColor;
				o.color = v.color;
				return o;
			}

			fixed4 frag (v2f i) : SV_TARGET
			{			
				return float4(YCbCrToRGB(SampleYCbCr( i.uvY, i.uvCbCr)).xyz, i.color.w);
			}
			ENDCG
		}
	}
}

