﻿using UnityEditor;
using System.Collections;
using System.IO;
using System;
using UnityEngine;

//This script expects the folder containing the StreamMeUp data to be selected in the editor
//When it is run, it'll populate the pointcloud object with all the frame files
public class FlipbookMenu : MonoBehaviour {

	//Create a menu item. When ran, this item expects that folder containing
	//all the frame data has been selected
	[MenuItem ("Flipbook/CreateFlipbook")]
	static void CreateFlipbook () 
	{

		//make sure we have just one thing selected
		if (Selection.objects.Length != 1) 
		{
			Debug.LogError ("Please select just the folder containing your data.");
			return;
		}

		//grab the folder that is selected
		Debug.Log( "Selection " + AssetDatabase.GetAssetPath(Selection.objects[0]) );
		string directory = AssetDatabase.GetAssetPath(Selection.objects[0]);
		DirectoryInfo dir = new DirectoryInfo( directory );

		//get all the textures
		FileInfo[] imageInfo = dir.GetFiles("*.png");
		if(imageInfo.Length == 0)
		{
			Debug.LogError("We are missing the data. Make sure the right folder is selected and run the script again");
			return;
		}

		//grab the point cloud object from the scene
        Flipbook p = GameObject.Find("Flipbook").GetComponent<Flipbook>();
		if (p == null) 
		{
			Debug.LogError ("Couldn't find the Flipbook object in the scene.");
			return;
		}

		//populate the point cloud object with the frame data
		p.sprites = new Sprite[imageInfo.Length];
		for(int i = 0; i < imageInfo.Length; i++)
		{
		
			FileInfo imageFile = imageInfo[i];
			string imageAssetName = directory + "/" + imageFile.Name;
			p.sprites[i] = (Sprite)AssetDatabase.LoadAssetAtPath(imageAssetName, typeof(Sprite));

		}
	}
}